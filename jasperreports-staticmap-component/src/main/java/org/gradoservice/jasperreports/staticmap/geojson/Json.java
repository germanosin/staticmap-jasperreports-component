package org.gradoservice.jasperreports.staticmap.geojson;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.io.StringWriter;

/**
 * Created by germanosin on 20.06.14.
 */
public class Json {
    private static final ObjectMapper defaultObjectMapper = new ObjectMapper();
    private static volatile ObjectMapper objectMapper = null;

    public static ObjectMapper mapper() {
        if (objectMapper == null) {
            return defaultObjectMapper;
        } else {
            return objectMapper;
        }
    }

    public static JsonNode parse(String src) {
        try {
            return mapper().readTree(src);
        } catch(Throwable t) {
            throw new RuntimeException(t);
        }
    }

    public static JsonNode toJson(final Object data) {
        try {
            return mapper().valueToTree(data);
        } catch(Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static String generateJson(Object o, boolean prettyPrint, boolean escapeNonASCII) {
        try {
            StringWriter sw = new StringWriter();
            JsonGenerator jgen = new JsonFactory(mapper()).createJsonGenerator(sw);
            if (prettyPrint) {
                jgen.setPrettyPrinter(new DefaultPrettyPrinter());
            }
            if (escapeNonASCII) {
                jgen.enable(JsonGenerator.Feature.ESCAPE_NON_ASCII);
            }
            mapper().writeValue(jgen, o);
            sw.flush();
            return sw.toString();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static String stringify(JsonNode json) {
        return generateJson(json, false, false);
    }
}
