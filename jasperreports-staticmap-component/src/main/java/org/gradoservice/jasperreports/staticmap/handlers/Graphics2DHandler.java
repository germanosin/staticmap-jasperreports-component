package org.gradoservice.jasperreports.staticmap.handlers;

import net.sf.jasperreports.engine.JRGenericPrintElement;
import net.sf.jasperreports.engine.JRPrintImage;
import net.sf.jasperreports.engine.export.GenericElementGraphics2DHandler;
import net.sf.jasperreports.engine.export.JRGraphics2DExporter;
import net.sf.jasperreports.engine.export.JRGraphics2DExporterContext;
import net.sf.jasperreports.engine.export.draw.ImageDrawer;
import net.sf.jasperreports.engine.export.draw.Offset;

import java.awt.*;

import static org.gradoservice.jasperreports.staticmap.StaticMapElementImageProvider.getImage;

/**
 * Created by germanosin on 17.06.14.
 */
public class Graphics2DHandler implements
        GenericElementGraphics2DHandler {

    private static final Graphics2DHandler INSTANCE =
            new Graphics2DHandler();

    public static Graphics2DHandler getInstance() {
        return INSTANCE;
    }

    @Override
    public void exportElement(JRGraphics2DExporterContext context,
                              JRGenericPrintElement element, Graphics2D grx, Offset offset) {
        try {
            JRGraphics2DExporter exporter = (JRGraphics2DExporter) context
                    .getExporter();
            ImageDrawer imageDrawer = exporter.getFrameDrawer().getDrawVisitor()
                    .getImageDrawer();
            JRPrintImage image = getImage(context.getJasperReportsContext(), element);
            imageDrawer.draw(grx, image, offset.getX(), offset.getY());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public boolean toExport(JRGenericPrintElement element) {
        return true;
    }
}
