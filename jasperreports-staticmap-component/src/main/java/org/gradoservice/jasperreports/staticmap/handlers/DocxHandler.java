package org.gradoservice.jasperreports.staticmap.handlers;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRGenericPrintElement;
import net.sf.jasperreports.engine.JRPrintImage;
import net.sf.jasperreports.engine.export.JRExporterGridCell;
import net.sf.jasperreports.engine.export.ooxml.GenericElementDocxHandler;
import net.sf.jasperreports.engine.export.ooxml.JRDocxExporter;
import net.sf.jasperreports.engine.export.ooxml.JRDocxExporterContext;
import org.gradoservice.jasperreports.staticmap.StaticMapElementImageProvider;

import java.io.IOException;

/**
 * Created by germanosin on 17.06.14.
 */
public class DocxHandler implements GenericElementDocxHandler {

    private static final DocxHandler INSTANCE = new DocxHandler();

    public static DocxHandler getInstance() {
        return INSTANCE;
    }

    @Override
    public void exportElement(JRDocxExporterContext exporterContext,
                              JRGenericPrintElement element, JRExporterGridCell gridCell) {
        JRDocxExporter exporter = (JRDocxExporter) exporterContext.getExporter();
        try {
            exporter.exportImage(exporterContext.getTableHelper(),
                    getImage(exporterContext, element), gridCell);
        } catch (JRException e) {
            throw new RuntimeException(e);
        }

    }

    @Override
    public boolean toExport(JRGenericPrintElement element) {
        return true;
    }

    @Override
    public JRPrintImage getImage(JRDocxExporterContext exporterContext,
                                 JRGenericPrintElement element) throws JRException
    {
            return StaticMapElementImageProvider.getImage(
                    exporterContext.getJasperReportsContext(), element);
    }
}
