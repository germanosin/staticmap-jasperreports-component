package org.gradoservice.jasperreports.staticmap.handlers;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRGenericPrintElement;
import net.sf.jasperreports.engine.JRPrintImage;
import net.sf.jasperreports.engine.export.JRExporterGridCell;
import net.sf.jasperreports.engine.export.JRGridLayout;
import net.sf.jasperreports.engine.export.oasis.GenericElementOdsHandler;
import net.sf.jasperreports.engine.export.oasis.JROdsExporter;
import net.sf.jasperreports.engine.export.oasis.JROdsExporterContext;
import org.gradoservice.jasperreports.staticmap.StaticMapElementImageProvider;

import java.io.IOException;

/**
 * Created by germanosin on 17.06.14.
 */
public class OdsHandler implements GenericElementOdsHandler {

    /** Singleton instance. */
    private static final OdsHandler INSTANCE = new OdsHandler();

    public static OdsHandler getInstance() {
        return INSTANCE;
    }

    public void exportElement(
            JROdsExporterContext exporterContext,
            JRGenericPrintElement element,
            JRExporterGridCell gridCell,
            int colIndex,
            int rowIndex,
            int emptyCols,
            int yCutsRow,
            JRGridLayout layout
    )
    {
        try
        {
            JROdsExporter exporter = (JROdsExporter)exporterContext.getExporter();
            exporter.exportImage(getImage(exporterContext, element), gridCell, colIndex, rowIndex, 0, 0, null);
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
    }

    @Override
    public JRPrintImage getImage(JROdsExporterContext exporterContext,
                                 JRGenericPrintElement element) throws JRException {
        return StaticMapElementImageProvider.getImage(exporterContext.getJasperReportsContext(), element);
    }

    public boolean toExport(JRGenericPrintElement element) {
        return true;
    }
}
