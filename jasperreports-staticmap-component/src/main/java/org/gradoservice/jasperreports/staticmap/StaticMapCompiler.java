package org.gradoservice.jasperreports.staticmap;

import net.sf.jasperreports.engine.JRDatasetRun;
import net.sf.jasperreports.engine.JRElementDataset;
import net.sf.jasperreports.engine.JRExpression;
import net.sf.jasperreports.engine.JRExpressionCollector;
import net.sf.jasperreports.engine.base.JRBaseObjectFactory;
import net.sf.jasperreports.engine.component.Component;
import net.sf.jasperreports.engine.component.ComponentCompiler;
import net.sf.jasperreports.engine.design.JRVerifier;
import net.sf.jasperreports.engine.type.EvaluationTimeEnum;
import org.gradoservice.jasperreports.staticmap.geojson.GeoJson;
import org.gradoservice.jasperreports.staticmap.geojson.GeoJsonData;
import org.gradoservice.jasperreports.staticmap.geojson.GeoJsonDataSet;
import org.gradoservice.jasperreports.staticmap.geojson.ItemProperty;

import java.util.List;

/**
 * {@link ComponentCompiler} implementation for Static map components.
 */
public class StaticMapCompiler implements ComponentCompiler {

    public static void collectExpressions(GeoJsonData data, JRExpressionCollector collector)
    {
        if (data != null)
        {
            JRExpressionCollector datasetCollector = collector;

            JRElementDataset dataset = data.getDataset();
            JRDatasetRun datasetRun = dataset == null ? null : dataset.getDatasetRun();
            if (datasetRun != null)
            {
                collector.collect(datasetRun);
                datasetCollector = collector.getDatasetCollector(datasetRun.getDatasetName());
            }

            List<GeoJson> items = data.getItems();
            if (items != null && !items.isEmpty())
            {
                for(GeoJson item : items)
                {
                    List<ItemProperty> itemProperties = item.getProperties();
                    if(itemProperties != null)
                    {
                        for(ItemProperty property : itemProperties)
                        {
                            datasetCollector.addExpression(property.getValueExpression());
                        }
                    }
                }
            }
        }
    }

    public static void collectExpressions(GeoJsonDataSet dataset, JRExpressionCollector collector)
    {
        if(dataset != null)
        {
            JRExpressionCollector datasetCollector = collector;

            JRDatasetRun datasetRun = dataset.getDatasetRun();
            if (datasetRun != null)
            {
                collector.collect(datasetRun);
                datasetCollector = collector.getDatasetCollector(datasetRun.getDatasetName());
            }

            List<GeoJson> geoJsons = dataset.getGeoJsons();
            if (geoJsons != null && !geoJsons.isEmpty())
            {
                for(GeoJson geoJson : geoJsons)
                {
                    List<ItemProperty> itemProperties = geoJson.getProperties();
                    if(itemProperties != null)
                    {
                        for(ItemProperty property : itemProperties)
                        {
                            datasetCollector.addExpression(property.getValueExpression());
                        }
                    }
                }
            }
        }
    }

    @Override
    public void collectExpressions(Component component,
                                   JRExpressionCollector collector) {
        StaticMapComponent map = (StaticMapComponent) component;
        collector.addExpression(map.getZoomExpression());
        collector.addExpression(map.getLatitudeExpression());
        collector.addExpression(map.getLongitudeExpression());

        List<GeoJsonData> geoJsonDataList = map.getGeoJsonDataList();
        if(geoJsonDataList != null && geoJsonDataList.size() > 0) {
            for(GeoJsonData geoJsonData : geoJsonDataList){
                collectExpressions(geoJsonData, collector);
            }
        }
    }



    @Override
    public Component toCompiledComponent(Component component,
                                         JRBaseObjectFactory baseFactory) {
        StaticMapComponent map = (StaticMapComponent) component;
        return new StandardStaticMapComponent(map, baseFactory);
    }

    @Override
    public void verify(Component component, JRVerifier verifier) {
        StaticMapComponent map = (StaticMapComponent) component;

        EvaluationTimeEnum evaluationTime = map.getEvaluationTime();
        if (evaluationTime == EvaluationTimeEnum.AUTO)
        {
            verifier.addBrokenRule("Auto evaluation time is not supported for maps", map);
        }
        else if (evaluationTime == EvaluationTimeEnum.GROUP)
        {
            String evaluationGroup = map.getEvaluationGroup();
            if (evaluationGroup == null || evaluationGroup.length() == 0)
            {
                verifier.addBrokenRule("No evaluation group set for map", map);
            }
            else if (!verifier.getReportDesign().getGroupsMap().containsKey(evaluationGroup))
            {
                verifier.addBrokenRule("Map evalution group \""
                        + evaluationGroup + " not found", map);
            }
        }

        verifyExpression(verifier, map, map.getLatitudeExpression(), "Latitude");
        verifyExpression(verifier, map, map.getLongitudeExpression(), "Longtitude");
        verifyExpression(verifier, map, map.getZoomExpression(), "Zoom");
    }

    private void verifyExpression(JRVerifier verifier, StaticMapComponent map,
                                  JRExpression jrExpression, String expressionName) {
        if (jrExpression == null || jrExpression.getText() == null
                || jrExpression.getText().trim().isEmpty()) {
            verifier.addBrokenRule(expressionName + " expression is empty", map);
        }
    }

}