package org.gradoservice.jasperreports.staticmap.geojson;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by germanosin on 20.06.14.
 */
public class FeatureGeoJson {
    private String type="Feature";
    private JsonNode geometry;
    private JsonNode style;
    private JsonNode properties;


    public FeatureGeoJson() {

    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public JsonNode getGeometry() {
        return geometry;
    }

    public void setGeometry(JsonNode geometry) {
        this.geometry = geometry;
    }

    public JsonNode getStyle() {
        return style;
    }

    public void setStyle(JsonNode style) {
        this.style = style;
    }

    public JsonNode getProperties() {
        return properties;
    }

    public void setProperties(JsonNode properties) {
        this.properties = properties;
    }

    public static String toFeatureGeoJson(String geometry, String style, String layerName) {
        Map<String, Object> properties = new HashMap<String, Object>();
        FeatureGeoJson featureGeoJson = new FeatureGeoJson();
        featureGeoJson.setGeometry(Json.parse(geometry));
        if (style!=null) featureGeoJson.setStyle(Json.parse(style));
        if (layerName!=null) {
            properties.put("name", layerName);
            featureGeoJson.setProperties(Json.toJson(properties));
        }
        return Json.stringify(Json.toJson(featureGeoJson));
    }
}
