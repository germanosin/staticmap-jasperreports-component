package org.gradoservice.jasperreports.staticmap.geojson;

import net.sf.jasperreports.engine.JRCloneable;
import net.sf.jasperreports.engine.JRDatasetRun;

import java.util.List;

/**
 * Created by germanosin on 19.06.14.
 */
public interface GeoJsonDataSet extends JRCloneable {
    public List<GeoJson> getGeoJsons();

    public JRDatasetRun getDatasetRun();
}
