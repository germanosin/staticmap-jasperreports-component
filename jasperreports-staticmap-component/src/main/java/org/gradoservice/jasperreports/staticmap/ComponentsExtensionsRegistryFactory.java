package org.gradoservice.jasperreports.staticmap;

import net.sf.jasperreports.engine.JRPropertiesMap;
import net.sf.jasperreports.engine.component.ComponentManager;
import net.sf.jasperreports.engine.component.ComponentsBundle;
import net.sf.jasperreports.engine.component.DefaultComponentXmlParser;
import net.sf.jasperreports.engine.component.DefaultComponentsBundle;
import net.sf.jasperreports.engine.export.*;
import net.sf.jasperreports.engine.export.oasis.JROdsExporter;
import net.sf.jasperreports.engine.export.oasis.JROdtExporter;
import net.sf.jasperreports.engine.export.ooxml.JRDocxExporter;
import net.sf.jasperreports.engine.export.ooxml.JRPptxExporter;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.extensions.ExtensionsRegistry;
import net.sf.jasperreports.extensions.ExtensionsRegistryFactory;
import org.gradoservice.jasperreports.staticmap.fill.FillFactory;
import org.gradoservice.jasperreports.staticmap.handlers.*;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/**
 * Created by germanosin on 17.06.14.
 */
public class ComponentsExtensionsRegistryFactory implements
        ExtensionsRegistryFactory {

    public static final String NAMESPACE =
            "http://gradoservice.com/jasperreports/components";
    public static final String XSD_LOCATION =
            "http://gradoservice.com/jasperreports/components/staticmap/xsd/components.xsd";
    public static final String XSD_RESOURCE =
            "org/gradoservice/jasperreports/staticmap/components.xsd";

    public static final String STATIC_MAP_COMPONENT_NAME = "staticmap";

    private static final ExtensionsRegistry REGISTRY;

    private static final GenericElementHandlerBundle HANDLER_BUNDLE =
            new GenericElementHandlerBundle() {
                @Override
                public String getNamespace() {
                    return ComponentsExtensionsRegistryFactory.NAMESPACE;
                }

                @Override
                public GenericElementHandler getHandler(String elementName,
                                                        String exporterKey) {
                    if (StaticMapPrintElement.STATIC_MAP_ELEMENT_NAME.equals(elementName)) {
                        if (JRGraphics2DExporter.GRAPHICS2D_EXPORTER_KEY
                                .equals(exporterKey)) {
                            return Graphics2DHandler.getInstance();
                        }
                        if (JRHtmlExporter.HTML_EXPORTER_KEY.equals(exporterKey)
                                || JRXhtmlExporter.XHTML_EXPORTER_KEY.equals(exporterKey)) {
                            return HtmlHandler.getInstance();
                        }
                        else if (JRPdfExporter.PDF_EXPORTER_KEY.equals(exporterKey)) {
                            return PdfHandler.getInstance();
                        }
                        else if (JRXlsExporter.XLS_EXPORTER_KEY.equals(exporterKey)) {
                            return XlsHandler.getInstance();
                        }
                        else if (JExcelApiExporter.JXL_EXPORTER_KEY.equals(exporterKey)) {
                            return JExcelApiHandler.getInstance();
                        }
                        else if (JRXlsxExporter.XLSX_EXPORTER_KEY.equals(exporterKey)) {
                            return XlsxHandler.getInstance();
                        }
                        else if (JRDocxExporter.DOCX_EXPORTER_KEY.equals(exporterKey)) {
                            return DocxHandler.getInstance();
                        }
                        else if (JRPptxExporter.PPTX_EXPORTER_KEY.equals(exporterKey)) {
                            return PptxHandler.getInstance();
                        }
                        else if (JRRtfExporter.RTF_EXPORTER_KEY.equals(exporterKey)) {
                            return RtfHandler.getInstance();
                        }
                        else if (JROdtExporter.ODT_EXPORTER_KEY.equals(exporterKey)) {
                            return OdtHandler.getInstance();
                        }
                        else if (JROdsExporter.ODS_EXPORTER_KEY.equals(exporterKey)) {
                            return OdsHandler.getInstance();
                        }
                    }
                    return null;
                }
            };

    private static final DefaultComponentsBundle COMPONENTS_BUNDLE = new DefaultComponentsBundle();

    static {
        DefaultComponentXmlParser parser = new DefaultComponentXmlParser();
        parser.setNamespace(NAMESPACE);
        parser.setPublicSchemaLocation(XSD_LOCATION);
        parser.setInternalSchemaResource(XSD_RESOURCE);
        parser.setDigesterConfigurer(new StaticMapComponentsXmlDigesterConfigurer());
        COMPONENTS_BUNDLE.setXmlParser(parser);

        StaticMapComponentManager staticMapManager = new StaticMapComponentManager();
        staticMapManager.setDesignConverter(StaticMapDesignConverter.getInstance());
        staticMapManager.setComponentCompiler(new StaticMapCompiler());
        staticMapManager.setComponentFillFactory(new FillFactory());

        HashMap<String, ComponentManager> componentManagers =
                new HashMap<String, ComponentManager>();
        componentManagers.put(STATIC_MAP_COMPONENT_NAME, staticMapManager);
        COMPONENTS_BUNDLE.setComponentManagers(componentManagers);
    }

    static {
        REGISTRY = new ExtensionsRegistry() {

            @SuppressWarnings("unchecked")
            @Override
            public <T> List<T> getExtensions(Class<T> extensionType) {
                if (extensionType == ComponentsBundle.class) {
                    return (List<T>) Collections.singletonList(COMPONENTS_BUNDLE);
                }
                if (extensionType == GenericElementHandlerBundle.class) {
                    return (List<T>) Collections.singletonList(HANDLER_BUNDLE);
                }
                return null;
            }
        };
    }

    @Override
    public ExtensionsRegistry createRegistry(String registryId,
                                             JRPropertiesMap properties) {
        return REGISTRY;
    }
}
