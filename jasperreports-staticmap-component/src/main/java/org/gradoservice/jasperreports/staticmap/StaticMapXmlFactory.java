package org.gradoservice.jasperreports.staticmap;

import net.sf.jasperreports.engine.type.EvaluationTimeEnum;
import net.sf.jasperreports.engine.xml.JRBaseFactory;
import net.sf.jasperreports.engine.xml.JRXmlConstants;
import org.xml.sax.Attributes;

/**
 * Created by germanosin on 17.06.14.
 */
public class StaticMapXmlFactory extends JRBaseFactory {


    @Override
    public Object createObject(Attributes atts) {
        StandardStaticMapComponent map = new StandardStaticMapComponent();

        // BaseMap attribute
        String baseMap = atts.getValue(StaticMapComponentsXmlWriter.ATTRIBUTE_BASE_MAP);
        if (baseMap != null) {
            map.setBaseMap(baseMap);
        }

        EvaluationTimeEnum evaluationTime = EvaluationTimeEnum.getByName(atts
                .getValue(JRXmlConstants.ATTRIBUTE_evaluationTime));
        if (evaluationTime != null) {
            map.setEvaluationTime(evaluationTime);
        }

        if (map.getEvaluationTime() == EvaluationTimeEnum.GROUP) {
            String groupName = atts
                    .getValue(JRXmlConstants.ATTRIBUTE_evaluationGroup);
            map.setEvaluationGroup(groupName);
        }

        // Service URL attribute
        String serviceUrl =
                atts.getValue(StaticMapComponentsXmlWriter.ATTRIBUTE_SERVICE_URL);
        if (serviceUrl != null) {
            map.setServiceUrl(serviceUrl);
        }

        String fitBounds = atts.getValue(StaticMapComponentsXmlWriter.ATTRIBUTE_FIT_BOUNDS);

        if (fitBounds!=null && fitBounds.equals("true")) {
            map.setFitBounds(true);
        } else {
            map.setFitBounds(false);
        }

        String legend = atts.getValue(StaticMapComponentsXmlWriter.ATTRIBUTE_LEGEND);

        if (legend!=null && legend.equals("true")) {
            map.setLegend(true);
        } else {
            map.setLegend(false);
        }


        return map;
    }
}
