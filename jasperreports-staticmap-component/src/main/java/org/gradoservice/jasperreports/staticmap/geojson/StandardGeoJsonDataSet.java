package org.gradoservice.jasperreports.staticmap.geojson;

import net.sf.jasperreports.engine.JRConstants;
import net.sf.jasperreports.engine.JRDatasetRun;
import net.sf.jasperreports.engine.JRExpressionCollector;
import net.sf.jasperreports.engine.JRRuntimeException;
import net.sf.jasperreports.engine.base.JRBaseObjectFactory;
import net.sf.jasperreports.engine.design.events.JRChangeEventsSupport;
import net.sf.jasperreports.engine.design.events.JRPropertyChangeSupport;
import net.sf.jasperreports.engine.util.JRCloneUtils;
import org.gradoservice.jasperreports.staticmap.StaticMapCompiler;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by germanosin on 19.06.14.
 */
public class StandardGeoJsonDataSet implements Serializable, GeoJsonDataSet, JRChangeEventsSupport
{//FIXMEMAP implement clone?

    private static final long serialVersionUID = JRConstants.SERIAL_VERSION_UID;
    public static final String PROPERTY_MARKER = "marker";
    public static final String PROPERTY_DATASET_RUN = "datasetRun";

    private List<GeoJson> geoJsonList = new ArrayList<GeoJson>();
    private JRDatasetRun datasetRun;

    private transient JRPropertyChangeSupport eventSupport;

    public StandardGeoJsonDataSet()
    {
    }

    public StandardGeoJsonDataSet(StandardGeoJsonDataSet dataset, JRBaseObjectFactory factory)
    {
        geoJsonList = getCompiledGeoJsons(dataset.getGeoJsons(), factory);
        datasetRun = factory.getDatasetRun(dataset.getDatasetRun());
    }

    private static List<GeoJson> getCompiledGeoJsons(List<GeoJson> geoJsons, JRBaseObjectFactory factory)
    {
        if (geoJsons == null)
        {
            return null;
        }

        List<GeoJson> compiledMarkers = new ArrayList<GeoJson>(geoJsons.size());
        for (Iterator<GeoJson> it = geoJsons.iterator(); it.hasNext();)
        {
            GeoJson geoJson = it.next();
            GeoJson compiledMarker = new StandardGeoJson(getCompiledProperties(geoJson.getProperties(), factory));
            compiledMarkers.add(compiledMarker);
        }
        return compiledMarkers;
    }

    private static List<ItemProperty> getCompiledProperties(List<ItemProperty> properties, JRBaseObjectFactory factory)
    {
        if (properties == null)
        {
            return null;
        }

        List<ItemProperty> compiledProperties = new ArrayList<ItemProperty>(properties.size());
        for (Iterator<ItemProperty> it = properties.iterator(); it.hasNext();)
        {
            ItemProperty property = it.next();
            ItemProperty compiledProperty = new StandardItemProperty(property.getName(), property.getValue(), factory.getExpression(property.getValueExpression()));
            compiledProperties.add(compiledProperty);
        }
        return compiledProperties;
    }

    public void collectExpressions(JRExpressionCollector collector) {
        StaticMapCompiler.collectExpressions(this, collector);
    }

    @Override
    public List<GeoJson> getGeoJsons() {
        return geoJsonList;
    }

    /**
     *
     */
    public void addMarker(GeoJson marker)
    {
        geoJsonList.add(marker);
        getEventSupport().fireCollectionElementAddedEvent(PROPERTY_MARKER, marker, geoJsonList.size() - 1);
    }

    /**
     *
     */
    public void addMarker(int index, GeoJson marker)
    {
        if(index >=0 && index < geoJsonList.size())
            geoJsonList.add(index, marker);
        else{
            geoJsonList.add(marker);
            index = geoJsonList.size() - 1;
        }
        getEventSupport().fireCollectionElementAddedEvent(PROPERTY_MARKER, geoJsonList, index);
    }

    /**
     *
     */
    public GeoJson removeMarker(GeoJson marker)
    {
        if (marker != null)
        {
            int idx = geoJsonList.indexOf(marker);
            if (idx >= 0)
            {
                geoJsonList.remove(idx);
                getEventSupport().fireCollectionElementRemovedEvent(PROPERTY_MARKER, marker, idx);
            }
        }
        return marker;
    }

    public JRDatasetRun getDatasetRun()
    {
        return datasetRun;
    }

    /**
     * Sets the subdataset run information that will be used to create the marker list.
     *
     * @param datasetRun the subdataset run information
     * @see #getDatasetRun()
     */
    public void setDatasetRun(JRDatasetRun datasetRun)
    {
        Object old = this.datasetRun;
        this.datasetRun = datasetRun;
        getEventSupport().firePropertyChange(PROPERTY_DATASET_RUN, old, this.datasetRun);
    }



    public JRPropertyChangeSupport getEventSupport()
    {
        synchronized (this)
        {
            if (eventSupport == null)
            {
                eventSupport = new JRPropertyChangeSupport(this);
            }
        }

        return eventSupport;
    }

    public Object clone()
    {
        StandardGeoJsonDataSet clone = null;
        try
        {
            clone = (StandardGeoJsonDataSet) super.clone();
        }
        catch (CloneNotSupportedException e)
        {
            // never
            throw new JRRuntimeException(e);
        }
        clone.datasetRun = JRCloneUtils.nullSafeClone(datasetRun);
        clone.geoJsonList = JRCloneUtils.cloneList(geoJsonList);
        clone.eventSupport = null;
        return clone;
    }

}
