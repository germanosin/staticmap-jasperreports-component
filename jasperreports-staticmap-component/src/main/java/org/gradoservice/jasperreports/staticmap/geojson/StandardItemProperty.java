package org.gradoservice.jasperreports.staticmap.geojson;

/**
 * Created by germanosin on 19.06.14.
 */
import java.io.Serializable;

import net.sf.jasperreports.engine.JRConstants;
import net.sf.jasperreports.engine.JRExpression;
import net.sf.jasperreports.engine.design.events.JRChangeEventsSupport;
import net.sf.jasperreports.engine.design.events.JRPropertyChangeSupport;
import net.sf.jasperreports.engine.util.JRCloneUtils;

/**
 * @author Teodor Danciu (teodord@users.sourceforge.net)
 * @version $Id: StandardItemProperty.java 6002 2013-03-20 08:15:32Z teodord $
 */
public class StandardItemProperty implements ItemProperty, JRChangeEventsSupport, Serializable
{

    private static final long serialVersionUID = JRConstants.SERIAL_VERSION_UID;

    public static final String PROPERTY_NAME = "name";
    public static final String PROPERTY_VALUE = "value";
    public static final String PROPERTY_VALUE_EXPRESSION = "valueExpression";

    private transient JRPropertyChangeSupport eventSupport;

    private String name;
    private String value;
    private JRExpression valueExpression;

    public StandardItemProperty()
    {
    }

    public StandardItemProperty(String name, String value, JRExpression valueExpression)
    {
        this.name = name;
        this.valueExpression = valueExpression;
        this.value = value;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        Object old = this.name;
        this.name = name;
        getEventSupport().firePropertyChange(PROPERTY_NAME, old, this.name);
    }

    public String getValue()
    {
        return value;
    }

    public void setValue(String value)
    {
        Object old = this.value;
        this.value = value;
        getEventSupport().firePropertyChange(PROPERTY_VALUE, old, this.value);
    }

    public JRExpression getValueExpression()
    {
        return valueExpression;
    }

    public void setValueExpression(JRExpression valueExpression)
    {
        Object old = this.valueExpression;
        this.valueExpression = valueExpression;
        getEventSupport().firePropertyChange(PROPERTY_VALUE_EXPRESSION, old, this.valueExpression);
    }

    public JRPropertyChangeSupport getEventSupport()
    {
        synchronized (this)
        {
            if (eventSupport == null)
            {
                eventSupport = new JRPropertyChangeSupport(this);
            }
        }

        return eventSupport;
    }

    public Object clone()
    {
        try
        {
            StandardItemProperty clone = (StandardItemProperty) super.clone();
            clone.valueExpression = JRCloneUtils.nullSafeClone(valueExpression);
            return clone;
        }
        catch (CloneNotSupportedException e)
        {
            // never
            throw new RuntimeException(e);
        }
    }
}