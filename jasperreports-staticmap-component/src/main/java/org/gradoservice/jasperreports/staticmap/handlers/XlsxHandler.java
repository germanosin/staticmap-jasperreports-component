package org.gradoservice.jasperreports.staticmap.handlers;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRGenericPrintElement;
import net.sf.jasperreports.engine.JRPrintImage;
import net.sf.jasperreports.engine.export.JRExporterGridCell;
import net.sf.jasperreports.engine.export.ooxml.GenericElementXlsxHandler;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporterContext;
import org.gradoservice.jasperreports.staticmap.StaticMapElementImageProvider;

import java.io.IOException;

/**
 * Created by germanosin on 17.06.14.
 */
public class XlsxHandler implements GenericElementXlsxHandler {
    private static final XlsxHandler INSTANCE = new XlsxHandler();

    public static XlsxHandler getInstance() {
        return INSTANCE;
    }

    @Override
    public void exportElement(JRXlsxExporterContext exporterContext,
                              JRGenericPrintElement element, JRExporterGridCell gridCell, int colIndex,
                              int rowIndex) {
        try {
            JRXlsxExporter exporter = (JRXlsxExporter) exporterContext.getExporter();
            JRPrintImage image = getImage(exporterContext, element);
            exporter.exportImage(image, gridCell,
                    colIndex, rowIndex, 0, 0, null);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public boolean toExport(JRGenericPrintElement element) {
        return true;
    }

    @Override
    public JRPrintImage getImage(JRXlsxExporterContext exporterContext,
                                 JRGenericPrintElement element) throws JRException {
            return StaticMapElementImageProvider
                    .getImage(exporterContext.getJasperReportsContext(), element);
    }
}
