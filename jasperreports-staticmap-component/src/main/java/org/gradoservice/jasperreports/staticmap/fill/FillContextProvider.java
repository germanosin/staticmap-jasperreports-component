package org.gradoservice.jasperreports.staticmap.fill;

import net.sf.jasperreports.engine.component.FillContext;


/**
 * @author Teodor Danciu (teodord@users.sourceforge.net)
 * @version $Id: FillContextProvider.java 6004 2013-03-20 12:49:30Z teodord $
 */
public interface FillContextProvider
{
    public FillContext getFillContext();
}