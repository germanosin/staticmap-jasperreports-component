package org.gradoservice.jasperreports.staticmap;


import net.sf.jasperreports.engine.JRElementDataset;
import net.sf.jasperreports.engine.JRExpression;
import net.sf.jasperreports.engine.component.XmlDigesterConfigurer;
import net.sf.jasperreports.engine.xml.JRExpressionFactory;
import net.sf.jasperreports.engine.xml.JRXmlConstants;
import org.apache.commons.digester.Digester;
import org.gradoservice.jasperreports.staticmap.geojson.*;

/**
 * Created by germanosin on 17.06.14.
 */
public class StaticMapComponentsXmlDigesterConfigurer  implements
        XmlDigesterConfigurer {
    @Override
    public void configureDigester(Digester digester) {
        addMapRules(digester);
    }

    protected void addMapRules(Digester digester) {

        String jrNamespace = JRXmlConstants.JASPERREPORTS_NAMESPACE;

        String mapPattern = "*/componentElement/staticmap";
        digester.addFactoryCreate(mapPattern, StaticMapXmlFactory.class);

        String zoomExpressionPattern = mapPattern + "/zoomExpression";
        digester.addFactoryCreate(zoomExpressionPattern,
                JRExpressionFactory.class.getName());
        digester.addCallMethod(zoomExpressionPattern, "setText", 0);
        digester.addSetNext(zoomExpressionPattern, "setZoomExpression",
                JRExpression.class.getName());

        String longitudeExpressionPattern = mapPattern + "/longitudeExpression";
        digester.addFactoryCreate(longitudeExpressionPattern,
                JRExpressionFactory.class.getName());
        digester.addCallMethod(longitudeExpressionPattern, "setText", 0);
        digester.addSetNext(longitudeExpressionPattern, "setLongitudeExpression",
                JRExpression.class.getName());

        String latitudeExpressionPattern = mapPattern + "/latitudeExpression";
        digester.addFactoryCreate(latitudeExpressionPattern,
                JRExpressionFactory.class.getName());
        digester.addCallMethod(latitudeExpressionPattern, "setText", 0);
        digester.addSetNext(latitudeExpressionPattern, "setLatitudeExpression",
                JRExpression.class.getName());



        String geoJsonDataPattern = mapPattern + "/geoJsonData";
        digester.addFactoryCreate(geoJsonDataPattern, GeoJsonDataXmlFactory.class.getName());
        digester.addSetNext(geoJsonDataPattern, "addGeoJsonData", GeoJsonData.class.getName());


        String geoJsonPattern = "*/geoItem";
        digester.addFactoryCreate(geoJsonPattern, GeoJsonXmlFactory.class.getName());
        digester.addSetNext(geoJsonPattern, "addItem", GeoJson.class.getName());

        String itemPropertyPattern = geoJsonPattern + "/itemProperty";
        digester.addFactoryCreate(itemPropertyPattern, ItemPropertyXmlFactory.class.getName());
        digester.addSetNext(itemPropertyPattern, "addItemProperty", ItemProperty.class.getName());

        digester.setRuleNamespaceURI(jrNamespace);

        String itemPropertyValueExpressionPattern = itemPropertyPattern + "/" + JRXmlConstants.ELEMENT_valueExpression;
        digester.addFactoryCreate(itemPropertyValueExpressionPattern, JRExpressionFactory.class.getName());
        digester.addCallMethod(itemPropertyValueExpressionPattern, "setText", 0);
        digester.addSetNext(itemPropertyValueExpressionPattern, "setValueExpression", JRExpression.class.getName());

        digester.setRuleNamespaceURI(jrNamespace);
        digester.addFactoryCreate(geoJsonDataPattern + "/dataset", GeoJsonDatasetFactory.class.getName());
        digester.addSetNext(geoJsonDataPattern + "/dataset", "setDataset", JRElementDataset.class.getName());

        String componentNamespace = digester.getRuleNamespaceURI();

        // leave the digester namespace in the same state
        digester.setRuleNamespaceURI(componentNamespace);
    }

}
