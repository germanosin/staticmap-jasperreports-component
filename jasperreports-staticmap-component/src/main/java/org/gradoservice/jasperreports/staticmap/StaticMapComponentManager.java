package org.gradoservice.jasperreports.staticmap;

import net.sf.jasperreports.engine.JasperReportsContext;
import net.sf.jasperreports.engine.component.ComponentXmlWriter;
import net.sf.jasperreports.engine.component.DefaultComponentManager;

/**
 * Created by germanosin on 17.06.14.
 */
public class StaticMapComponentManager extends DefaultComponentManager {

    @Override
    public ComponentXmlWriter getComponentXmlWriter(
            JasperReportsContext jasperReportsContext) {
        return new StaticMapComponentsXmlWriter(jasperReportsContext);
    }
}
