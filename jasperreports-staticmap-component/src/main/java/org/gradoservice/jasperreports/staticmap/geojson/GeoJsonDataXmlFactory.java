package org.gradoservice.jasperreports.staticmap.geojson;

import net.sf.jasperreports.engine.xml.JRBaseFactory;
import org.xml.sax.Attributes;

/**
 * Created by germanosin on 19.06.14.
 */
public class GeoJsonDataXmlFactory extends JRBaseFactory
{

    public Object createObject(Attributes attrs) throws Exception
    {
        StandardGeoJsonData dataset = new StandardGeoJsonData();
        return dataset;
    }

}
