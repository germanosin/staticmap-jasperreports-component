/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.gradoservice.jasperreports.staticmap;

import net.sf.jasperreports.engine.JRCloneable;
import net.sf.jasperreports.engine.JRExpression;
import net.sf.jasperreports.engine.component.Component;
import net.sf.jasperreports.engine.type.EvaluationTimeEnum;
import org.gradoservice.jasperreports.staticmap.geojson.GeoJsonData;
import org.gradoservice.jasperreports.staticmap.geojson.GeoJsonDataSet;

import java.util.List;

/**
 *
 * @author germanosin
 */
public interface StaticMapComponent extends Component, JRCloneable {
    String getBaseMap();
    JRExpression getZoomExpression();
    JRExpression getLongitudeExpression();
    JRExpression getLatitudeExpression();

    EvaluationTimeEnum getEvaluationTime();
    String getEvaluationGroup();
    String getServiceUrl();
    Boolean getFitBounds();
    Boolean getLegend();

    List<GeoJsonData> getGeoJsonDataList();
}
