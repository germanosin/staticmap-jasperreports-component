package org.gradoservice.jasperreports.staticmap;

import net.sf.jasperreports.engine.JRGenericElementType;

/**
 * Created by germanosin on 17.06.14.
 */
public class StaticMapPrintElement {
    public static final String STATIC_MAP_ELEMENT_NAME = "staticmap";

    public static final JRGenericElementType STATIC_MAP_ELEMENT_TYPE =
            new JRGenericElementType(
                    ComponentsExtensionsRegistryFactory.NAMESPACE,
                    STATIC_MAP_ELEMENT_NAME);

    /** Parameter {@code baseMap}. */
    public static final String PARAMETER_BASE_MAP = "baseMap";

    /** Parameter {@code longitude}. */
    public static final String PARAMETER_LONGITUDE = "longitude";

    /** Parameter {@code latitude}. */
    public static final String PARAMETER_LATITUDE = "latitude";

    /** Parameter {@code zoom}. */
    public static final String PARAMETER_ZOOM = "zoom";

    /** Parameter {@code urlParameters}. */
    public static final String PARAMETER_URL_PARAMETERS = "urlParameters";

    /** Parameter {@code cacheRenderer}. */
    public static final String PARAMETER_CACHE_RENDERER = "cacheRenderer";

    public static final String PARAMETER_GEOJSONS = "geoJsons";

    public static final String PARAMETER_GEOJSON_ELEMENT = "geojson";



    private StaticMapPrintElement()
    {
    }
}
