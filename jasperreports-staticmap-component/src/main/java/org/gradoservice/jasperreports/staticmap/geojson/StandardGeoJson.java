package org.gradoservice.jasperreports.staticmap.geojson;

import net.sf.jasperreports.engine.JRConstants;
import net.sf.jasperreports.engine.design.events.JRChangeEventsSupport;
import net.sf.jasperreports.engine.design.events.JRPropertyChangeSupport;
import net.sf.jasperreports.engine.util.JRCloneUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by germanosin on 19.06.14.
 */
public class StandardGeoJson implements GeoJson, JRChangeEventsSupport, Serializable
{

    private static final long serialVersionUID = JRConstants.SERIAL_VERSION_UID;
    public static final String PROPERTY_ITEM_PROPERTIES = "itemProperties";

    private transient JRPropertyChangeSupport eventSupport;

    private List<ItemProperty> properties = new ArrayList<ItemProperty>();

    public StandardGeoJson()
    {
    }

    public StandardGeoJson(List<ItemProperty> properties)
    {
        this.properties = properties;
    }

    public JRPropertyChangeSupport getEventSupport()
    {
        synchronized (this)
        {
            if (eventSupport == null)
            {
                eventSupport = new JRPropertyChangeSupport(this);
            }
        }

        return eventSupport;
    }

    public Object clone()
    {
        StandardGeoJson clone = null;
        try
        {
            clone = (StandardGeoJson) super.clone();
        }
        catch (CloneNotSupportedException e)
        {
            // never
            throw new RuntimeException(e);
        }
        clone.properties = JRCloneUtils.cloneList(properties);
        return clone;
    }

    @Override
    public List<ItemProperty> getProperties()
    {
        return properties;
    }

    public void addItemProperty(ItemProperty property)
    {
        properties.add(property);
        getEventSupport().fireCollectionElementAddedEvent(PROPERTY_ITEM_PROPERTIES, property, properties.size() - 1);
    }

    public void removeItemProperty(ItemProperty property)
    {
        int idx = properties.indexOf(property);
        if (idx >= 0)
        {
            properties.remove(idx);

            getEventSupport().fireCollectionElementRemovedEvent(PROPERTY_ITEM_PROPERTIES,
                    property, idx);
        }
    }
}

