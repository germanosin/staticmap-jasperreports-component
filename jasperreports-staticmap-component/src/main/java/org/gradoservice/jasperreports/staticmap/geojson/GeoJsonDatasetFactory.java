package org.gradoservice.jasperreports.staticmap.geojson;

import net.sf.jasperreports.engine.design.JRDesignElementDataset;
import net.sf.jasperreports.engine.xml.JRElementDatasetFactory;

/**
 * Created by germanosin on 19.06.14.
 */
public class GeoJsonDatasetFactory extends JRElementDatasetFactory
{

    @Override
    public JRDesignElementDataset getDataset()
    {
        return new JRDesignElementDataset();
    }

}
