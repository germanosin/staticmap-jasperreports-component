package org.gradoservice.jasperreports.staticmap.geojson;

import net.sf.jasperreports.engine.JRConstants;
import net.sf.jasperreports.engine.JRElementDataset;
import net.sf.jasperreports.engine.JRExpressionCollector;
import net.sf.jasperreports.engine.JRRuntimeException;
import net.sf.jasperreports.engine.base.JRBaseObjectFactory;
import net.sf.jasperreports.engine.design.events.JRChangeEventsSupport;
import net.sf.jasperreports.engine.design.events.JRPropertyChangeSupport;
import net.sf.jasperreports.engine.util.JRCloneUtils;
import org.gradoservice.jasperreports.staticmap.StaticMapCompiler;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by germanosin on 19.06.14.
 */
public class StandardGeoJsonData implements Serializable, GeoJsonData, JRChangeEventsSupport {
    private static final long serialVersionUID = JRConstants.SERIAL_VERSION_UID;
    public static final String PROPERTY_ITEMS = "geojsons";
    public static final String PROPERTY_DATASET = "dataset";

    private List<GeoJson> itemsList = new ArrayList<GeoJson>();
    private JRElementDataset dataset;

    private transient JRPropertyChangeSupport eventSupport;

    public StandardGeoJsonData()
    {
    }

    public StandardGeoJsonData(GeoJsonData data, JRBaseObjectFactory factory)
    {
        itemsList = getCompiledItems(data.getItems(), factory);

        dataset = factory.getElementDataset(data.getDataset());
    }

    private static List<GeoJson> getCompiledItems(List<GeoJson> items, JRBaseObjectFactory factory)
    {
        if (items == null)
        {
            return null;
        }

        List<GeoJson> compiledItems = new ArrayList<GeoJson>(items.size());
        for (Iterator<GeoJson> it = items.iterator(); it.hasNext();)
        {
            GeoJson item = it.next();
            GeoJson compiledItem = new StandardGeoJson(getCompiledProperties(item.getProperties(), factory));
            compiledItems.add(compiledItem);
        }
        return compiledItems;
    }

    private static List<ItemProperty> getCompiledProperties(List<ItemProperty> properties, JRBaseObjectFactory factory)
    {
        if (properties == null)
        {
            return null;
        }

        List<ItemProperty> compiledProperties = new ArrayList<ItemProperty>(properties.size());
        for (Iterator<ItemProperty> it = properties.iterator(); it.hasNext();)
        {
            ItemProperty property = it.next();
            ItemProperty compiledProperty = new StandardItemProperty(property.getName(), property.getValue(), factory.getExpression(property.getValueExpression()));
            compiledProperties.add(compiledProperty);
        }
        return compiledProperties;
    }

    public void collectExpressions(JRExpressionCollector collector) {
        StaticMapCompiler.collectExpressions(this, collector);
    }

    @Override
    public List<GeoJson> getItems() {
        return itemsList;
    }

    /**
     *
     */
    public void addItem(GeoJson item)
    {
        itemsList.add(item);
        getEventSupport().fireCollectionElementAddedEvent(PROPERTY_ITEMS, item, itemsList.size() - 1);
    }

    /**
     *
     */
    public void addItem(int index, GeoJson item)
    {
        if(index >=0 && index < itemsList.size())
            itemsList.add(index, item);
        else{
            itemsList.add(item);
            index = itemsList.size() - 1;
        }
        getEventSupport().fireCollectionElementAddedEvent(PROPERTY_ITEMS, itemsList, index);
    }

    /**
     *
     */
    public GeoJson removeItem(GeoJson item)
    {
        if (item != null)
        {
            int idx = itemsList.indexOf(item);
            if (idx >= 0)
            {
                itemsList.remove(idx);
                getEventSupport().fireCollectionElementRemovedEvent(PROPERTY_ITEMS, item, idx);
            }
        }
        return item;
    }

    @Override
    public JRElementDataset getDataset()
    {
        return dataset;
    }

    /**
     * Sets the dataset information that will be used to create the item list.
     *
     * @param dataset the dataset information
     * @see #getDataset()
     */
    public void setDataset(JRElementDataset dataset)
    {
        Object old = this.dataset;
        this.dataset = dataset;
        getEventSupport().firePropertyChange(PROPERTY_DATASET, old, this.dataset);
    }

    public JRPropertyChangeSupport getEventSupport()
    {
        synchronized (this)
        {
            if (eventSupport == null)
            {
                eventSupport = new JRPropertyChangeSupport(this);
            }
        }

        return eventSupport;
    }

    public Object clone()
    {
        StandardGeoJsonData clone = null;
        try
        {
            clone = (StandardGeoJsonData) super.clone();
        }
        catch (CloneNotSupportedException e)
        {
            // never
            throw new JRRuntimeException(e);
        }
        clone.dataset = JRCloneUtils.nullSafeClone(dataset);
        clone.itemsList = JRCloneUtils.cloneList(itemsList);
        clone.eventSupport = null;
        return clone;
    }
}
