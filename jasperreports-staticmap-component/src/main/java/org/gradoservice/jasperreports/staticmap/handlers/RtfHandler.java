package org.gradoservice.jasperreports.staticmap.handlers;

import net.sf.jasperreports.engine.JRGenericPrintElement;
import net.sf.jasperreports.engine.JRPrintImage;
import net.sf.jasperreports.engine.export.GenericElementRtfHandler;
import net.sf.jasperreports.engine.export.JRRtfExporter;
import net.sf.jasperreports.engine.export.JRRtfExporterContext;
import org.gradoservice.jasperreports.staticmap.StaticMapElementImageProvider;

/**
 * Created by germanosin on 17.06.14.
 */
public class RtfHandler implements GenericElementRtfHandler {

    private static final RtfHandler INSTANCE = new RtfHandler();

    public static RtfHandler getInstance() {
        return INSTANCE;
    }

    @Override
    public void exportElement(JRRtfExporterContext exporterContext,
                              JRGenericPrintElement element) {
        try {
            JRRtfExporter exporter = (JRRtfExporter) exporterContext.getExporter();
            JRPrintImage image = StaticMapElementImageProvider.getImage(
                    exporterContext.getJasperReportsContext(), element);
            exporter.exportImage(image);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public boolean toExport(JRGenericPrintElement element) {
        return true;
    }
}
