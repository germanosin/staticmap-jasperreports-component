package org.gradoservice.jasperreports.staticmap.handlers;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRGenericPrintElement;
import net.sf.jasperreports.engine.JRPrintImage;
import net.sf.jasperreports.engine.export.ooxml.GenericElementPptxHandler;
import net.sf.jasperreports.engine.export.ooxml.JRPptxExporter;
import net.sf.jasperreports.engine.export.ooxml.JRPptxExporterContext;
import org.gradoservice.jasperreports.staticmap.StaticMapElementImageProvider;

import java.io.IOException;

/**
 * Created by germanosin on 17.06.14.
 */
public class PptxHandler implements GenericElementPptxHandler {

    private static final PptxHandler INSTANCE = new PptxHandler();

    public static PptxHandler getInstance() {
        return INSTANCE;
    }

    @Override
    public void exportElement(JRPptxExporterContext exporterContext,
                              JRGenericPrintElement element) {
        try {
            JRPptxExporter exporter = (JRPptxExporter) exporterContext.getExporter();
            exporter.exportImage(getImage(exporterContext, element));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public boolean toExport(JRGenericPrintElement element) {
        return true;
    }

    @Override
    public JRPrintImage getImage(JRPptxExporterContext exporterContext,
                                 JRGenericPrintElement element) throws JRException {
            return StaticMapElementImageProvider.getImage(
                    exporterContext.getJasperReportsContext(), element);
    }
}
