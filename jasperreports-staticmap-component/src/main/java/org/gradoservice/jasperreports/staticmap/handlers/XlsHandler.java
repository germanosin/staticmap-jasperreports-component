package org.gradoservice.jasperreports.staticmap.handlers;

import net.sf.jasperreports.engine.JRGenericPrintElement;
import net.sf.jasperreports.engine.JRPrintImage;
import net.sf.jasperreports.engine.export.*;

import static org.gradoservice.jasperreports.staticmap.StaticMapElementImageProvider.getImage;

/**
 * Created by germanosin on 17.06.14.
 */
public class XlsHandler implements GenericElementXlsHandler {
    private static final XlsHandler INSTANCE = new XlsHandler();

    public static XlsHandler getInstance() {
        return INSTANCE;
    }

    @Override
    public void exportElement(JRXlsExporterContext exporterContext,
                              JRGenericPrintElement element, JRExporterGridCell gridCell, int colIndex,
                              int rowIndex, int emptyCols, int yCutsRow, JRGridLayout layout
    ) {
        try {
            JRXlsExporter exporter = (JRXlsExporter) exporterContext.getExporter();
            JRPrintImage image = getImage(exporterContext.getJasperReportsContext(),
                    element);
            exporter.exportImage(image, gridCell, colIndex, rowIndex, emptyCols,
                    yCutsRow, layout);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public boolean toExport(JRGenericPrintElement element) {
        return true;
    }
}
