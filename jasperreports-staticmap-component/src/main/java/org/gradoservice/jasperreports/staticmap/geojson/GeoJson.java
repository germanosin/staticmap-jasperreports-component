package org.gradoservice.jasperreports.staticmap.geojson;

import net.sf.jasperreports.engine.JRCloneable;

import java.util.List;

/**
 * Created by germanosin on 19.06.14.
 */
public interface GeoJson extends JRCloneable {
    public static final String PROPERTY_geojson = "geojson";
    public static final String PROPERTY_layerName="layerName";
    public static final String PROPERTY_style="style";
    public List<ItemProperty> getProperties();
}
