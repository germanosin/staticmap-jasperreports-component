package org.gradoservice.jasperreports.staticmap.fill;

/**
 * Created by germanosin on 19.06.14.
 */
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


import net.sf.jasperreports.engine.JRElementDataset;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.fill.JRFillExpressionEvaluator;
import net.sf.jasperreports.engine.fill.JRFillObjectFactory;
import org.gradoservice.jasperreports.staticmap.geojson.GeoJson;
import org.gradoservice.jasperreports.staticmap.geojson.GeoJsonData;


/**
 * @author Teodor Danciu (teodord@users.sourceforge.net)
 * @version $Id: FillItemData.java 6688 2013-11-06 14:27:23Z shertage $
 */
public class FillGeoItemData
{

    /**
     *
     */
    protected GeoJsonData itemData;
    protected List<FillGeoJson> itemsList;
    protected FillItemDataset fillItemDataset;
    protected FillContextProvider fillContextProvider;
    private List<Map<String,Object>> evaluatedItems = null;

    /**
     *
     */
    public FillGeoItemData(
            FillContextProvider fillContextProvider,
            GeoJsonData itemData,
            JRFillObjectFactory factory
    )// throws JRException
    {
        factory.put(itemData, this);

        this.itemData = itemData;
        this.fillContextProvider = fillContextProvider;

        if (itemData.getDataset() != null)
        {
            fillItemDataset = new FillItemDataset(this, factory);
        }

		/*   */
        List<GeoJson> srcItemList = itemData.getItems();
        if (srcItemList != null && !srcItemList.isEmpty())
        {
            itemsList = new ArrayList<FillGeoJson>();
            for(GeoJson item : srcItemList)
            {
                if(item != null)
                {
                    itemsList.add(getFillItem(item, factory));
                }
            }
        }
    }

    /**
     *
     */
    public JRElementDataset getDataset()
    {
        return itemData.getDataset();
    }

    /**
     *
     */
    public void evaluateItems(JRFillExpressionEvaluator evaluator, byte evaluation) throws JRException
    {
        if (itemsList != null)
        {
            for(FillGeoJson item : itemsList)
            {
                item.evaluateProperties(evaluator, evaluation);
            }
        }
    }

    /**
     *
     */
    public List<Map<String,Object>> getEvaluateItems(byte evaluation) throws JRException
    {
        if (fillItemDataset != null)
        {
            fillItemDataset.setEvaluation(evaluation);
            fillItemDataset.evaluateDatasetRun(evaluation);
        }

        if (itemsList != null && fillItemDataset==null)
        {
            evaluateItems(fillContextProvider.getFillContext(), evaluation);
            addEvaluateItems();
        }

        return evaluatedItems;
    }

    /**
     *
     */
    public void addEvaluateItems()
    {
        if (itemsList != null)
        {
            if (evaluatedItems == null || getDataset() == null)
            {
                evaluatedItems = new ArrayList<Map<String,Object>>();
            }

            for(FillGeoJson item : itemsList)
            {
                evaluatedItems.add(item.getEvaluatedProperties());
            }
        }
    }

    public FillGeoJson getFillItem(GeoJson item, JRFillObjectFactory factory) {
        return new FillGeoJson(item, factory);
    };
}
