package org.gradoservice.jasperreports.staticmap.fill;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.fill.JRFillExpressionEvaluator;
import net.sf.jasperreports.engine.fill.JRFillObjectFactory;
import org.gradoservice.jasperreports.staticmap.geojson.GeoJson;
import org.gradoservice.jasperreports.staticmap.geojson.ItemProperty;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by germanosin on 19.06.14.
 */
public class FillGeoJson implements GeoJson {
    /**
     *
     */
    protected GeoJson item;
    protected Map<String, Object> evaluatedProperties;

    /**
     *
     */
    public FillGeoJson(
            GeoJson item,
            JRFillObjectFactory factory
    )
    {
        factory.put(item, this);

        this.item = item;
    }


    /**
     *
     */
    public void evaluateProperties(JRFillExpressionEvaluator evaluator, byte evaluation) throws JRException
    {
        List<ItemProperty> itemProperties = getProperties();
        Map<String, Object> result = null;
        if(itemProperties != null && !itemProperties.isEmpty())
        {
            result = new HashMap<String, Object>();
            for(ItemProperty property : itemProperties)
            {
                result.put(property.getName(), getEvaluatedValue(property, evaluator, evaluation));
            }
        }

        //if some of the item properties are conditioning each other
        verifyValues(result);
        evaluatedProperties = result;
    }


    /**
     *
     */
    public Object clone()
    {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<ItemProperty> getProperties()
    {
        return item.getProperties();
    }

    public Map<String, Object> getEvaluatedProperties()
    {
        return evaluatedProperties;
    }

    public Object getEvaluatedValue(ItemProperty property, JRFillExpressionEvaluator evaluator, byte evaluation) throws JRException
    {
        Object result = null;
        if(property.getValueExpression() == null || "".equals(property.getValueExpression()))
        {
            result = property.getValue();
        }
        else
        {
            result = evaluator.evaluate(property.getValueExpression(), evaluation);
        }

        verifyValue(property, result);

        return result;
    }

    public void verifyValue(ItemProperty property, Object value) throws JRException {

    };

    public void verifyValues(Map<String, Object> result) throws JRException {

    };
}
