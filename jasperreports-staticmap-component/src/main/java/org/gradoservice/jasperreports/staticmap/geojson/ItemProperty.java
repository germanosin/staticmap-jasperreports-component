package org.gradoservice.jasperreports.staticmap.geojson;

import net.sf.jasperreports.engine.JRCloneable;
import net.sf.jasperreports.engine.JRExpression;

/**
 * Created by germanosin on 19.06.14.
 */
public interface ItemProperty extends JRCloneable
{

    /**
     * Returns the name of the item property (required).
     * @return the property name
     */
    String getName();

    /**
     * Returns the <code>value</code> attribute of the item property. Only
     * <code>java.lang.String</code> values are allowed for this attribute.
     * @return the <code>value</code> attribute
     */
    String getValue();

    /**
     * Returns a {@link net.sf.jasperreports.engine.JRExpression JRExpression} representing
     * the value object for the item property. If present, it overrides the value given by
     * the <code>value</code> attribute.
     *
     * @return the value expression
     */
    JRExpression getValueExpression();

}
