package org.gradoservice.jasperreports.staticmap.handlers;

import net.sf.jasperreports.engine.JRGenericPrintElement;
import net.sf.jasperreports.engine.JRPrintImage;
import net.sf.jasperreports.engine.JasperReportsContext;
import net.sf.jasperreports.engine.export.*;

import static org.gradoservice.jasperreports.staticmap.StaticMapElementImageProvider.getImage;

/**
 * Created by germanosin on 17.06.14.
 */
public class JExcelApiHandler implements
        GenericElementJExcelApiHandler {

    /** Singleton instance. */
    private static final JExcelApiHandler INSTANCE =
            new JExcelApiHandler();

    public static JExcelApiHandler getInstance() {
        return INSTANCE;
    }

    @Override
    public void exportElement(JExcelApiExporterContext exporterContext,
                              JRGenericPrintElement element, JRExporterGridCell gridCell,
                              int colIndex, int rowIndex, int emptyCols, int yCutsRow,
                              JRGridLayout layout) {
        try {
            JExcelApiExporter exporter = (JExcelApiExporter) exporterContext
                    .getExporter();
            JasperReportsContext reportsContext = exporterContext
                    .getJasperReportsContext();
            JRPrintImage printImage = getImage(reportsContext, element);
            exporter.exportImage(printImage, gridCell, colIndex, rowIndex, emptyCols,
                    yCutsRow, layout);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public boolean toExport(JRGenericPrintElement element) {
        return true;
    }
}
