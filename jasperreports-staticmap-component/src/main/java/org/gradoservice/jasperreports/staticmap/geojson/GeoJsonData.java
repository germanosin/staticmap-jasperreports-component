package org.gradoservice.jasperreports.staticmap.geojson;

import net.sf.jasperreports.engine.JRCloneable;
import net.sf.jasperreports.engine.JRElementDataset;

import java.util.List;

/**
 * Created by germanosin on 19.06.14.
 */
public interface GeoJsonData extends JRCloneable {
    public List<GeoJson> getItems();
    public JRElementDataset getDataset();
}
