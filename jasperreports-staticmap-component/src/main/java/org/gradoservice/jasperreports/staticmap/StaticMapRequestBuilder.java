package org.gradoservice.jasperreports.staticmap;



import net.sf.jasperreports.engine.util.Pair;
import org.gradoservice.jasperreports.staticmap.geojson.GeoJsonData;

import static java.net.URLEncoder.encode;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.*;

/**
 * Created by germanosin on 17.06.14.
 */
public class StaticMapRequestBuilder {

    private static final String UTF_8 = "UTF-8";

    private final Map<StaticMapRequestParameter, String> parameters =
            new HashMap<StaticMapRequestParameter, String>();

    private List<String> geoJsonList;

    public static StaticMapRequestBuilder createRequest(String serviceUrl) {
        StaticMapRequestBuilder builder = new StaticMapRequestBuilder();
        builder.parameters.put(StaticMapRequestParameter.serviceUrl, serviceUrl);
        return builder;
    }

    public static StaticMapRequestBuilder createRequest(
            Map<StaticMapRequestParameter, String> params) {
        StaticMapRequestBuilder builder = new StaticMapRequestBuilder();
        builder.parameters.putAll(params);
        return builder;
    }

    public StaticMapRequestBuilder baseMap(String baseMap) {
        parameters.put(StaticMapRequestParameter.baseMap, baseMap);
        return this;
    }

    public StaticMapRequestBuilder fitBounds(Boolean fitBounds) {
        parameters.put(StaticMapRequestParameter.fitBounds, fitBounds.toString());
        return this;
    }

    public StaticMapRequestBuilder legend(Boolean legend) {
        parameters.put(StaticMapRequestParameter.legend, legend.toString());
        return this;
    }

    public StaticMapRequestBuilder center(float longitude, float latitude) {
        parameters.put(StaticMapRequestParameter.center, Double.valueOf(latitude).toString()+","+Double.valueOf(longitude).toString());
        return this;
    }

    public StaticMapRequestBuilder zoom(Integer zoom) {
        parameters.put(StaticMapRequestParameter.zoom, zoom.toString());
        return this;
    }


    public StaticMapRequestBuilder size(int width, int height) {
        parameters
                .put(StaticMapRequestParameter.size, Integer.valueOf(width).toString()+"x"+Integer.valueOf(height).toString());
        return this;
    }

    public StaticMapRequestBuilder geoJsonDataList(List<String> geoJsons) {
        this.geoJsonList = geoJsons;
        return this;
    }

    public String getPostParameters() throws UnsupportedEncodingException {
        List<Pair<String,String>> params = new ArrayList<Pair<String,String>>();
        if (geoJsonList!=null && !geoJsonList.isEmpty()) {
            for (String geoJson : geoJsonList) {
                Pair<String,String> currentGeoJson = new Pair<String, String>("geoJsons[]", geoJson);
                params.add(currentGeoJson);
            }
        }
        return getQuery(params);
    }

    private String getQuery(List<Pair<String,String>> params) throws UnsupportedEncodingException
    {
        StringBuilder result = new StringBuilder();
        boolean first = true;

        for (Pair<String,String> pair : params)
        {
            if (first)
                first = false;
            else
                result.append("&");

            result.append(pair.first());
            result.append("=");
            result.append(URLEncoder.encode(pair.second(), "UTF-8"));
        }

        return result.toString();
    }



    public URL toMapUrl() throws MalformedURLException {
        StringBuilder url = new StringBuilder();
        Map<StaticMapRequestParameter, String> params =
                new LinkedHashMap<StaticMapRequestParameter, String>(this.parameters);
        String serviceUrl = params.remove(StaticMapRequestParameter.serviceUrl).toString();
        url.append(serviceUrl);
        if (serviceUrl.indexOf("?") == -1) {
            url.append("?");
        }
        java.util.List<String> paramList = new ArrayList<String>();

        for (StaticMapRequestParameter parameter : StaticMapRequestParameter.parameterValues()) {
            if (parameter == StaticMapRequestParameter.URL_PARAMETERS) {
                continue;
            }
            Object defaultValue = parameter.defaultValue(params);
            if (params.containsKey(parameter) || parameter.isRequired(params)
                    || defaultValue != null) {
                String parameterName = parameter.parameterName(params);
                Object value = parameter.extract(params);
                paramList.add(encodeParameter(parameterName, value.toString()));
            }
        }
        Iterator<String> iterator = paramList.iterator();
        while (iterator.hasNext()) {
            String param = iterator.next();
            url.append(param);
            if (iterator.hasNext()) {
                url.append("&");
            }
        }
        Object urlParams = params.remove(StaticMapRequestParameter.URL_PARAMETERS);
        if (urlParams != null) {
            String extraUrlParams = urlParams.toString();
            if (!extraUrlParams.startsWith("&")) {
                url.append("&");
            }
            url.append(extraUrlParams);
        }
        return new URL(url.toString());
    }

    private static String encodeParameter(String key, Object value) {
        try {
            String encName = encode(key, UTF_8);
            String encValue = encode(value.toString(), UTF_8);
            String encodedParameter = String.format("%s=%s", encName, encValue);
            return encodedParameter;
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("Encoding UTF-8 not supported on runtime");
        }
    }
}
