package org.gradoservice.jasperreports.staticmap;

import static java.lang.String.format;

import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.base.JRBasePrintImage;
import net.sf.jasperreports.engine.type.HorizontalAlignEnum;
import net.sf.jasperreports.engine.type.OnErrorTypeEnum;
import net.sf.jasperreports.engine.type.ScaleImageEnum;
import net.sf.jasperreports.engine.type.VerticalAlignEnum;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Created by germanosin on 17.06.14.
 */
public class StaticMapElementImageProvider {

    private static final Charset UTF_8 = Charset.forName("UTF-8");

    private static final Log log = LogFactory.getLog(StaticMapElementImageProvider.class);

    public static JRPrintImage getImage(
            JasperReportsContext jasperReportsContext, JRGenericPrintElement element)
            throws JRException {

        JRBasePrintImage printImage = new JRBasePrintImage(
                element.getDefaultStyleProvider());

        printImage.setUUID(element.getUUID());
        printImage.setX(element.getX());
        printImage.setY(element.getY());
        printImage.setWidth(element.getWidth());
        printImage.setHeight(element.getHeight());
        printImage.setStyle(element.getStyle());
        printImage.setMode(element.getModeValue());
        printImage.setBackcolor(element.getBackcolor());
        printImage.setForecolor(element.getForecolor());
        printImage.setLazy(false);
        printImage.setScaleImage(ScaleImageEnum.CLIP);
        printImage.setHorizontalAlignment(HorizontalAlignEnum.LEFT);
        printImage.setVerticalAlignment(VerticalAlignEnum.TOP);

        Renderable cacheRenderer = (Renderable) element
                .getParameterValue(StaticMapPrintElement.PARAMETER_CACHE_RENDERER);

        if (cacheRenderer == null) {
            try {
                cacheRenderer = getImageRenderable(jasperReportsContext, element);
            } catch (IOException e) {
                throw  new JRException(e);
            }
            element.setParameterValue(StaticMapPrintElement.PARAMETER_CACHE_RENDERER,
                    cacheRenderer);
        }

        printImage.setRenderable(cacheRenderer);

        return printImage;
    }

    public static Renderable getImageRenderable(
            JasperReportsContext jasperReportsContext, JRGenericPrintElement element)
            throws IOException, JRException {
        StaticMapRequestBuilder requestBuilder = mapRequestBuilder(element);
        return getImageRenderable(jasperReportsContext, element.getKey(),
                requestBuilder);
    }

    public static Renderable getImageRenderable(
            JasperReportsContext jasperReportsContext, String elementName,
            StaticMapRequestBuilder requestBuilder)
            throws IOException, JRException {

        URL mapUrl = requestBuilder.toMapUrl();
        HttpURLConnection httpConnection = (HttpURLConnection) mapUrl
                .openConnection();
        httpConnection.setRequestMethod("POST");
        httpConnection.setDoInput(true);
        httpConnection.setDoOutput(true);

        OutputStream os = httpConnection.getOutputStream();
        BufferedWriter writer = new BufferedWriter(
                new OutputStreamWriter(os, "UTF-8"));
        writer.write(requestBuilder.getPostParameters());
        writer.flush();
        writer.close();
        os.close();

        httpConnection.connect();

        // Handle JSON response
        validateServerResponse(elementName, mapUrl, httpConnection);

        JasperReportsContext context = jasperReportsContext;
        if (context == null) {
            context = DefaultJasperReportsContext.getInstance();
        }
        Renderable cacheRenderer = RenderableUtil.getInstance(context)
                .getRenderable(
                        httpConnection.getInputStream(),
                        OnErrorTypeEnum.ERROR);
        // cacheRenderer.getImageData(jasperReportsContext);
        return cacheRenderer;
    }

    static void validateServerResponse(String elementName, URL mapUrl,
                                       HttpURLConnection httpConnection)
            throws IOException, JRException {
        String contentType = httpConnection.getContentType();
        if (contentType == null) {
            throw new JRException(format("Unable to determine content type, URL: %s",
                    mapUrl));
        }
        if (httpConnection.getResponseCode() != HttpURLConnection.HTTP_OK) {
            if (contentType.contains("json")) {
                evaluateServiceExceptionReport(elementName, mapUrl, httpConnection);
                return;
            }
            throw new JRException(format("Unable to load Static map for report element:"
                            + " %s, server responded: %s, URL: %s", elementName,
                    httpConnection.getResponseMessage(), mapUrl));
        }
        if (contentType.contains("json")) {
            evaluateServiceExceptionReport(elementName, mapUrl, httpConnection);
        }
    }

    static void evaluateServiceExceptionReport(String elementName, URL mapUrl,
                                               HttpURLConnection httpConnection)
            throws IOException, JRException {
        String encodingName = httpConnection.getContentEncoding();
        Charset encoding;
        try {
            encoding = Charset.forName(encodingName);
        } catch (IllegalArgumentException e) {
            encoding = UTF_8;
        }
        /*ServiceExceptionReport report = parseExceptionReportFromStream(
                httpConnection.getInputStream(), encoding);
        ServiceException ex = report.getServiceException();
        throw new JRException(format("The server reported an exception for "
                        + "WMS map element '%s', code: %s, message: %s, URL: %s",
                elementName, ex.getCode(), ex.getBody(), mapUrl));*/
    }

    public static StaticMapRequestBuilder mapRequestBuilder(JRGenericPrintElement element) {
        Map<StaticMapRequestParameter, String> params = new HashMap<StaticMapRequestParameter, String>();
        for (StaticMapRequestParameter param : StaticMapRequestParameter.values()) {
            Object value = element.getParameterValue(param.name());
            if (value != null) {
                params.put(param, value.toString());
            }
        }

        List<Map<String,Object>> geoJsonList = (List<Map<String,Object>>)element.getParameterValue(StaticMapPrintElement.PARAMETER_GEOJSONS);
        String geoJsonListInfo = geoJsonList==null ? "null" : geoJsonList.isEmpty() ? "empty" : "has elements";
        log.warn("geoJson List is "+geoJsonListInfo);
        List<String> geoJsons = new ArrayList<String>();
        if(geoJsonList != null && !geoJsonList.isEmpty()) {
            for (Map<String, Object> map : geoJsonList) {
                if(map != null && !map.isEmpty())
                {
                    String geoJson = (String)map.get(StaticMapPrintElement.PARAMETER_GEOJSON_ELEMENT);
                    geoJsons.add(geoJson);
                }
            }
        }


        StaticMapRequestBuilder requestBuilder = StaticMapRequestBuilder
                .createRequest(params)
                .geoJsonDataList(geoJsons)
                .size(element.getWidth(), element.getHeight());

        return requestBuilder;
    }
}
