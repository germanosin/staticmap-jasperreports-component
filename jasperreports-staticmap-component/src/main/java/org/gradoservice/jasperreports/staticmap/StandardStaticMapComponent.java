/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.gradoservice.jasperreports.staticmap;
/**
 *
 * @author germanosin
 */

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import net.sf.jasperreports.engine.JRConstants;
import net.sf.jasperreports.engine.JRExpression;
import net.sf.jasperreports.engine.JRRuntimeException;
import net.sf.jasperreports.engine.base.JRBaseObjectFactory;
import net.sf.jasperreports.engine.design.events.JRChangeEventsSupport;
import net.sf.jasperreports.engine.design.events.JRPropertyChangeSupport;
import net.sf.jasperreports.engine.type.EvaluationTimeEnum;
import net.sf.jasperreports.engine.util.JRCloneUtils;
import org.gradoservice.jasperreports.staticmap.geojson.GeoJsonData;
import org.gradoservice.jasperreports.staticmap.geojson.StandardGeoJsonData;

public class StandardStaticMapComponent implements StaticMapComponent, Serializable,
        JRChangeEventsSupport  {

    private static final long serialVersionUID = JRConstants.SERIAL_VERSION_UID;
    public static final String PROPERTY_SERVICE_URL = "serviceUrl";
    public static final String PROPERTY_BASEMAP = "baseMap";
    public static final String PROPERTY_ZOOM_EXPRESSION = "zoom";
    public static final String PROPERTY_LATITUDE_EXPRESSION = "lat";
    public static final String PROPERTY_LONGTITUDE_EXPRESSION = "lng";
    public static final String PROPERTY_GEOJSON_DATALIST = "geojsonDataList";
    public static final String PROPERTY_FIT_BOUNDS = "fitBounds";
    public static final String PROPERTY_LEGEND = "legend";


    public static final String PROPERTY_EVALATION_TIME = "evaluationTime";
    public static final String PROPERTY_EVALUATION_GROUP = "evaluationGroup";

    private String baseMap;
    private String serviceUrl;
    private JRExpression zoomExpression;
    private JRExpression longitudeExpression;
    private JRExpression latitudeExpression;
    private Boolean fitBounds;
    private Boolean legend;

    private List<GeoJsonData> geoJsonDataList = new ArrayList<GeoJsonData>();

    private EvaluationTimeEnum evaluationTime = EvaluationTimeEnum.NOW;
    private String evaluationGroup;

    private transient JRPropertyChangeSupport eventSupport;

    public StandardStaticMapComponent() {
    }

    public StandardStaticMapComponent(StaticMapComponent map,
                                      JRBaseObjectFactory objectFactory) {
        this.serviceUrl = map.getServiceUrl();
        this.baseMap = map.getBaseMap();
        this.zoomExpression = objectFactory.getExpression(map
                .getZoomExpression());
        this.longitudeExpression = objectFactory.getExpression(map.getLongitudeExpression());
        this.latitudeExpression = objectFactory.getExpression(map.getLatitudeExpression());
        this.evaluationTime = map.getEvaluationTime();
        this.evaluationGroup = map.getEvaluationGroup();
        this.fitBounds = map.getFitBounds();
        this.legend = map.getLegend();

        List<GeoJsonData> geoJsonList = map.getGeoJsonDataList();
        if(geoJsonList != null && geoJsonList.size() > 0)
        {
            this.geoJsonDataList = new ArrayList<GeoJsonData>();
            for(GeoJsonData geoJsonData : geoJsonList){
                this.geoJsonDataList.add(new StandardGeoJsonData(geoJsonData, objectFactory));
            }
        }
    }

    public String getBaseMap() {
        return this.baseMap;
    }

    public void setBaseMap(String baseMap) {
        Object old = this.baseMap;
        this.baseMap = baseMap;
        getEventSupport().firePropertyChange(PROPERTY_BASEMAP, old,
                this.baseMap);
    }

    public String getServiceUrl() {
        return serviceUrl;
    }

    public void setServiceUrl(String serviceUrl) {
        Object old = this.serviceUrl;
        this.serviceUrl = serviceUrl;
        getEventSupport().firePropertyChange(PROPERTY_SERVICE_URL, old,
                this.serviceUrl);
    }

    public JRExpression getZoomExpression() {
        return this.zoomExpression;
    }


    public void setZoomExpression(JRExpression zoom) {
        Object old = this.zoomExpression;
        this.zoomExpression = zoom;
        getEventSupport().firePropertyChange(PROPERTY_ZOOM_EXPRESSION, old,
                this.zoomExpression);
    }

    @Override
    public EvaluationTimeEnum getEvaluationTime() {
        return evaluationTime;
    }

    public void setEvaluationTime(EvaluationTimeEnum time) {
        Object old = this.evaluationTime;
        this.evaluationTime = time;
        getEventSupport().firePropertyChange(PROPERTY_EVALATION_TIME, old,
                this.evaluationTime);
    }


    public String getEvaluationGroup() {
        return evaluationGroup;
    }

    public void setEvaluationGroup(String evaluation) {
        Object old = this.evaluationGroup;
        this.evaluationGroup = evaluation;
        getEventSupport().firePropertyChange(PROPERTY_EVALUATION_GROUP, old,
                this.evaluationGroup);
    }

    public JRExpression getLatitudeExpression() {
        return this.latitudeExpression;
    }

    public void setLatitudeExpression(JRExpression center) {
        Object old = this.latitudeExpression;
        this.latitudeExpression = center;
        getEventSupport().firePropertyChange(PROPERTY_LATITUDE_EXPRESSION, old,
                this.latitudeExpression);
    }

    public JRExpression getLongitudeExpression() {
        return this.longitudeExpression;
    }

    public void setLongitudeExpression(JRExpression center) {
        Object old = this.longitudeExpression;
        this.longitudeExpression = center;
        getEventSupport().firePropertyChange(PROPERTY_LONGTITUDE_EXPRESSION, old,
                this.longitudeExpression);
    }

    @Override
    public List<GeoJsonData> getGeoJsonDataList() {
        return this.geoJsonDataList;
    }

    public void addGeoJsonData(GeoJsonData geoJsonData)
    {
        geoJsonDataList.add(geoJsonData);
        getEventSupport().fireCollectionElementAddedEvent(PROPERTY_GEOJSON_DATALIST, geoJsonData, geoJsonDataList.size() - 1);
    }

    public Boolean getFitBounds() {return this.fitBounds; }

    public void setFitBounds(Boolean fit) {
        Object old = this.fitBounds;
        this.fitBounds = fit;
        getEventSupport().firePropertyChange(PROPERTY_FIT_BOUNDS, old,
                this.fitBounds);
    }

    public Boolean getLegend() {return this.legend; }

    public void setLegend(Boolean legend) {
        Object old = this.legend;
        this.legend = legend;
        getEventSupport().firePropertyChange(PROPERTY_LEGEND, old,
                this.legend);
    }

    /**
     *
     */
    public void addGeoJsonData(int index, GeoJsonData geoJsonData)
    {
        if(index >=0 && index < geoJsonDataList.size())
            geoJsonDataList.add(index, geoJsonData);
        else{
            geoJsonDataList.add(geoJsonData);
            index = geoJsonDataList.size() - 1;
        }
        getEventSupport().fireCollectionElementAddedEvent(PROPERTY_GEOJSON_DATALIST, geoJsonDataList, index);
    }

    /**
     *
     */
    public GeoJsonData removeMarkerData(GeoJsonData geoJsonData)
    {
        if (geoJsonData != null)
        {
            int idx = geoJsonDataList.indexOf(geoJsonData);
            if (idx >= 0)
            {
                geoJsonDataList.remove(idx);
                getEventSupport().fireCollectionElementRemovedEvent(PROPERTY_GEOJSON_DATALIST, geoJsonData, idx);
            }
        }
        return geoJsonData;
    }



    @Override
    public JRPropertyChangeSupport getEventSupport() {
        synchronized (this) {
            if (eventSupport == null) {
                eventSupport = new JRPropertyChangeSupport(this);
            }
        }
        return eventSupport;
    }

    @Override
    public Object clone() {
        StandardStaticMapComponent clone = null;
        try {
            clone = (StandardStaticMapComponent) super.clone();
        } catch (CloneNotSupportedException e) {
            // never
            throw new JRRuntimeException(e);
        }
        clone.zoomExpression = JRCloneUtils.nullSafeClone(zoomExpression);
        clone.longitudeExpression = JRCloneUtils.nullSafeClone(longitudeExpression);
        clone.latitudeExpression = JRCloneUtils.nullSafeClone(latitudeExpression);
        clone.geoJsonDataList = JRCloneUtils.cloneList(this.geoJsonDataList);
        clone.eventSupport = null;
        return clone;
    }

}
