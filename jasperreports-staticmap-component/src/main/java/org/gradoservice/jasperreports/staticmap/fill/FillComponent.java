package org.gradoservice.jasperreports.staticmap.fill;

import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.component.BaseFillComponent;
import net.sf.jasperreports.engine.component.FillContext;
import net.sf.jasperreports.engine.component.FillPrepareResult;
import net.sf.jasperreports.engine.fill.JRFillObjectFactory;
import net.sf.jasperreports.engine.fill.JRTemplateGenericElement;
import net.sf.jasperreports.engine.fill.JRTemplateGenericPrintElement;
import net.sf.jasperreports.engine.type.EvaluationTimeEnum;
import org.gradoservice.jasperreports.staticmap.StaticMapComponent;
import org.gradoservice.jasperreports.staticmap.StaticMapPrintElement;
import org.gradoservice.jasperreports.staticmap.StaticMapRequestBuilder;
import org.gradoservice.jasperreports.staticmap.StaticMapRequestParameter;
import org.gradoservice.jasperreports.staticmap.geojson.FeatureGeoJson;
import org.gradoservice.jasperreports.staticmap.geojson.GeoJson;
import org.gradoservice.jasperreports.staticmap.geojson.GeoJsonData;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import static org.gradoservice.jasperreports.staticmap.StaticMapElementImageProvider.getImageRenderable;
import static org.gradoservice.jasperreports.staticmap.StaticMapRequestBuilder.createRequest;

/**
 * Created by germanosin on 17.06.14.
 */
public class FillComponent extends BaseFillComponent implements FillContextProvider {

    private static final Log log = LogFactory.getLog(FillComponent.class);

    JRFillObjectFactory factory;

    private final StaticMapComponent mapComponent;

    private String baseMap;
    private Integer zoom;
    private Float latitude;
    private Float longitude;
    private String urlParameters;
    private String serviceUrl;
    private Boolean fitBounds;
    private Boolean legend;
    private List<Map<String,Object>> geoJsons;

    private List<FillGeoItemData> geoJsonDataList;

    private Renderable imageRenderable;

    public FillComponent(StaticMapComponent map) {
        this.mapComponent = map;
    }

    public FillComponent(StaticMapComponent map, JRFillObjectFactory factory) {
        this.mapComponent = map;
        this.factory = factory;

        if(mapComponent.getGeoJsonDataList() != null){
            geoJsonDataList = new ArrayList<FillGeoItemData>();
            for(GeoJsonData geoJsonData : mapComponent.getGeoJsonDataList()) {
                geoJsonDataList.add(new FillGeoItemData(this, geoJsonData, factory));
            }
        }
    }

    protected StaticMapComponent getMap() {
        return mapComponent;
    }

    protected boolean isEvaluateNow() {
        return mapComponent.getEvaluationTime() == EvaluationTimeEnum.NOW;
    }

    @Override
    public void evaluate(byte evaluation) throws JRException {
        if (isEvaluateNow()) {
            evaluateMap(evaluation);
        }
    }

    void evaluateMap(byte evaluation) throws JRException {
        String elementName = fillContext.getComponentElement().getKey();
        int width = fillContext.getComponentElement().getWidth();
        int height = fillContext.getComponentElement().getHeight();

        this.baseMap = mapComponent.getBaseMap();
        this.serviceUrl = mapComponent.getServiceUrl();
        this.fitBounds = mapComponent.getFitBounds();
        this.legend = mapComponent.getLegend();
        this.longitude = (Float) fillContext.evaluate(
                mapComponent.getLongitudeExpression(), evaluation);
        this.latitude = (Float) fillContext.evaluate(
                mapComponent.getLatitudeExpression(), evaluation);
        this.zoom = (Integer) fillContext.evaluate(
                mapComponent.getZoomExpression(), evaluation);

        List<String> geoJsonsQuery = new ArrayList<String>();
        if(geoJsonDataList != null) {
            geoJsons = new ArrayList<Map<String,Object>>();

            for(FillGeoItemData geoJsonData : geoJsonDataList) {
                List<Map<String,Object>> currentItemList = geoJsonData.getEvaluateItems(evaluation);
                if(currentItemList != null && !currentItemList.isEmpty()){
                    for(Map<String,Object> currentItem : currentItemList){
                        if(currentItem != null){
                            geoJsons.add(currentItem);

                            String geoJson = (String)currentItem.get(GeoJson.PROPERTY_geojson);
                            String layerName = (String)currentItem.get(GeoJson.PROPERTY_layerName);
                            String style = (String)currentItem.get(GeoJson.PROPERTY_style);

                            if (geoJson!=null) {
                                geoJsonsQuery.add(FeatureGeoJson.toFeatureGeoJson(geoJson,style,layerName));
                            }

                        }
                    }
                }
            }
        }




        StaticMapRequestBuilder requestBuilder = createRequest(serviceUrl);
        requestBuilder
                .baseMap(baseMap)
                .center(longitude,latitude)
                .zoom(zoom)
                .fitBounds(fitBounds)
                .legend(legend)
                .geoJsonDataList(geoJsonsQuery)
                .size(width, height);

        // load map image
        try {
            this.imageRenderable = getImageRenderable(null, elementName,
                    requestBuilder);
        } catch (MalformedURLException e) {
            throw new JRException(e.getMessage(), e);
        } catch (IOException e) {
            throw new JRException(e.getMessage(), e);
        } catch (IllegalStateException e) {
            throw new JRException(e.getMessage(), e);
        }
    }


    @Override
    public FillPrepareResult prepare(int availableHeight) {
        return FillPrepareResult.PRINT_NO_STRETCH;
    }

    @Override
    public JRPrintElement fill() {
        JRComponentElement element = fillContext.getComponentElement();
        JRTemplateGenericElement template = new JRTemplateGenericElement(
                fillContext.getElementOrigin(),
                fillContext.getDefaultStyleProvider(),
                StaticMapPrintElement.STATIC_MAP_ELEMENT_TYPE);
        template = deduplicate(template);

        JRTemplateGenericPrintElement printElement = new JRTemplateGenericPrintElement(
                template, elementId);
        printElement.setUUID(element.getUUID());
        printElement.setX(element.getX());
        printElement.setY(fillContext.getElementPrintY());
        printElement.setWidth(element.getWidth());
        printElement.setHeight(element.getHeight());

        if (isEvaluateNow()) {
            copy(printElement);
        } else {
            fillContext.registerDelayedEvaluation(printElement,
                    mapComponent.getEvaluationTime(), mapComponent.getEvaluationGroup());
        }

        return printElement;
    }

    @Override
    public void evaluateDelayedElement(JRPrintElement element, byte evaluation)
            throws JRException {
        evaluateMap(evaluation);
        copy((JRGenericPrintElement) element);
    }

    protected void copy(JRGenericPrintElement printElement) {
        printElement.setParameterValue(
                StaticMapRequestParameter.baseMap.name(), baseMap);
        printElement.setParameterValue(StaticMapPrintElement.PARAMETER_LATITUDE, latitude);
        printElement.setParameterValue(StaticMapPrintElement.PARAMETER_LONGITUDE, longitude);
        printElement.setParameterValue(StaticMapPrintElement.PARAMETER_ZOOM, zoom);
        printElement.setParameterValue(StaticMapRequestParameter.serviceUrl.name(),
                serviceUrl);

        if (urlParameters != null) {
            printElement.setParameterValue(
                    StaticMapRequestParameter.URL_PARAMETERS.name(), urlParameters);
        }
        if (this.imageRenderable != null) {
            printElement.setParameterValue(
                    StaticMapPrintElement.PARAMETER_CACHE_RENDERER, this.imageRenderable);
        }

        if(geoJsons != null && !geoJsons.isEmpty())
        {
            printElement.setParameterValue(StaticMapPrintElement.PARAMETER_GEOJSONS, geoJsons);
        }
    }

    public FillContext getFillContext()
    {
        return fillContext;
    }
}
