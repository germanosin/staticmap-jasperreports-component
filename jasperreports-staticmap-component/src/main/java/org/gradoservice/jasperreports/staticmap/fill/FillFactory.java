package org.gradoservice.jasperreports.staticmap.fill;

import net.sf.jasperreports.engine.component.Component;
import net.sf.jasperreports.engine.component.ComponentFillFactory;
import net.sf.jasperreports.engine.fill.JRFillCloneFactory;
import net.sf.jasperreports.engine.fill.JRFillObjectFactory;
import org.gradoservice.jasperreports.staticmap.StaticMapComponent;

/**
 * Created by germanosin on 17.06.14.
 */
public class FillFactory implements ComponentFillFactory {
    @Override
    public net.sf.jasperreports.engine.component.FillComponent toFillComponent(Component component,
                                         JRFillObjectFactory factory) {
        StaticMapComponent map = (StaticMapComponent) component;
        return new FillComponent(map, factory);
    }

    @Override
    public net.sf.jasperreports.engine.component.FillComponent cloneFillComponent(net.sf.jasperreports.engine.component.FillComponent component,
                                            JRFillCloneFactory factory) {
        FillComponent fillMap = (FillComponent) component;
        return new FillComponent(fillMap.getMap());
    }
}
