package org.gradoservice.jasperreports.staticmap.geojson;

import net.sf.jasperreports.engine.xml.JRBaseFactory;
import net.sf.jasperreports.engine.xml.JRXmlConstants;

import org.xml.sax.Attributes;


/**
 * @author Teodor Danciu (teodord@users.sourceforge.net)
 * @version $Id: ItemPropertyXmlFactory.java 6002 2013-03-20 08:15:32Z teodord $
 */
public class ItemPropertyXmlFactory extends JRBaseFactory
{

    /**
     *
     */
    public Object createObject(Attributes atts)
    {
        StandardItemProperty itemProperty = new StandardItemProperty();

        String name = atts.getValue(JRXmlConstants.ATTRIBUTE_name);
        if(name != null)
        {
            itemProperty.setName(name);
        }
        String value = atts.getValue(JRXmlConstants.ATTRIBUTE_value);
        if(value != null)
        {
            itemProperty.setValue(value);
        }
        return itemProperty;
    }
}
