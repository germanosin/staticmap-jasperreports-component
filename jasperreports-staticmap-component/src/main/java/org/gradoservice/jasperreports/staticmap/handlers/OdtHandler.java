package org.gradoservice.jasperreports.staticmap.handlers;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRGenericPrintElement;
import net.sf.jasperreports.engine.JRPrintImage;
import net.sf.jasperreports.engine.export.JRExporterGridCell;
import net.sf.jasperreports.engine.export.oasis.GenericElementOdtHandler;
import net.sf.jasperreports.engine.export.oasis.JROdtExporter;
import net.sf.jasperreports.engine.export.oasis.JROdtExporterContext;
import org.gradoservice.jasperreports.staticmap.StaticMapElementImageProvider;

import java.io.IOException;

/**
 * Created by germanosin on 17.06.14.
 */
public class OdtHandler implements GenericElementOdtHandler {

    private static final OdtHandler INSTANCE = new OdtHandler();

    public static OdtHandler getInstance() {
        return INSTANCE;
    }

    @Override
    public void exportElement(JROdtExporterContext exporterContext,
                              JRGenericPrintElement element, JRExporterGridCell gridCell) {
        try {
            JROdtExporter exporter = (JROdtExporter) exporterContext.getExporter();
            exporter.exportImage(exporterContext.getTableBuilder(),
                    getImage(exporterContext, element), gridCell);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public JRPrintImage getImage(JROdtExporterContext exporterContext,
                                 JRGenericPrintElement element) throws JRException {
            return StaticMapElementImageProvider.getImage(
                    exporterContext.getJasperReportsContext(), element);
    }

    @Override
    public boolean toExport(JRGenericPrintElement element) {
        return true;
    }
}
