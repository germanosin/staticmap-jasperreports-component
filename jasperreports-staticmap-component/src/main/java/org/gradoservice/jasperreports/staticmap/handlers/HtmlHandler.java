package org.gradoservice.jasperreports.staticmap.handlers;

import net.sf.jasperreports.engine.JRGenericPrintElement;
import net.sf.jasperreports.engine.export.GenericElementHtmlHandler;
import net.sf.jasperreports.engine.export.JRHtmlExporter;
import net.sf.jasperreports.engine.export.JRHtmlExporterContext;
import net.sf.jasperreports.engine.export.JRXhtmlExporter;
import net.sf.jasperreports.engine.type.ModeEnum;
import net.sf.jasperreports.engine.util.JRColorUtil;
import net.sf.jasperreports.web.util.VelocityUtil;
import org.gradoservice.jasperreports.staticmap.StaticMapElementImageProvider;
import org.gradoservice.jasperreports.staticmap.StaticMapRequestBuilder;

import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by germanosin on 17.06.14.
 */
public class HtmlHandler implements GenericElementHtmlHandler {

    private static final HtmlHandler INSTANCE = new HtmlHandler();

    private static final String MAP_ELEMENT_HTML_TEMPLATE =
            "org/gradoservice/jasperreports/staticmap/resources/templates/StaticMapElementTemplate.vm";

    public static HtmlHandler getInstance() {
        return INSTANCE;
    }

    @Override
    public String getHtmlFragment(JRHtmlExporterContext context,
                                  JRGenericPrintElement element) {

        Map<String, Object> contextMap = new HashMap<String, Object>();
        contextMap.put("mapCanvasId", "map_canvas_" + element.hashCode());

        if (context.getExporter() instanceof JRXhtmlExporter) {
            contextMap.put("xhtml", "xhtml");
            JRXhtmlExporter xhtmlExporter = (JRXhtmlExporter) context.getExporter();
            contextMap.put("elementX", xhtmlExporter.toSizeUnit(element.getX()));
            contextMap.put("elementY", xhtmlExporter.toSizeUnit(element.getY()));
        } else {
            JRHtmlExporter htmlExporter = (JRHtmlExporter) context.getExporter();
            contextMap.put("elementX", htmlExporter.toSizeUnit(element.getX()));
            contextMap.put("elementY", htmlExporter.toSizeUnit(element.getY()));
        }
        contextMap.put("elementId", element.getKey());
        contextMap.put("elementWidth", element.getWidth());
        contextMap.put("elementHeight", element.getHeight());

        if (element.getModeValue() == ModeEnum.OPAQUE) {
            contextMap.put("backgroundColor",
                    JRColorUtil.getColorHexa(element.getBackcolor()));
        }
        StaticMapRequestBuilder requestBuilder =
                StaticMapElementImageProvider.mapRequestBuilder(element);
        try {
            contextMap.put("staticMapUrl", requestBuilder.toMapUrl());
        } catch (MalformedURLException e) {
            throw new RuntimeException("Unable to build WMS map service url", e);
        }

        return VelocityUtil.processTemplate(MAP_ELEMENT_HTML_TEMPLATE, contextMap);
    }

    @Override
    public boolean toExport(JRGenericPrintElement element) {
        return true;
    }
}
