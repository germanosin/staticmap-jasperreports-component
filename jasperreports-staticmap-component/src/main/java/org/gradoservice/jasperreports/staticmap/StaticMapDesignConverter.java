package org.gradoservice.jasperreports.staticmap;

import net.sf.jasperreports.engine.JRComponentElement;
import net.sf.jasperreports.engine.JRElement;
import net.sf.jasperreports.engine.JRPrintElement;
import net.sf.jasperreports.engine.component.ComponentDesignConverter;
import net.sf.jasperreports.engine.convert.ElementIconConverter;
import net.sf.jasperreports.engine.convert.ReportConverter;
import net.sf.jasperreports.engine.util.JRImageLoader;

/**
 * Created by germanosin on 17.06.14.
 */
public class StaticMapDesignConverter extends ElementIconConverter implements
        ComponentDesignConverter {

    private final static StaticMapDesignConverter INSTANCE = new StaticMapDesignConverter();

    private StaticMapDesignConverter() {
        super(JRImageLoader.SUBREPORT_IMAGE_RESOURCE);
    }

    public static StaticMapDesignConverter getInstance() {
        return INSTANCE;
    }

    @Override
    public JRPrintElement convert(ReportConverter reportConverter,
                                  JRComponentElement element) {
        return convert(reportConverter, (JRElement) element);
    }
}
