package org.gradoservice.jasperreports.staticmap.handlers;

import net.sf.jasperreports.engine.JRGenericPrintElement;
import net.sf.jasperreports.engine.JRPrintImage;
import net.sf.jasperreports.engine.export.GenericElementPdfHandler;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRPdfExporterContext;
import org.gradoservice.jasperreports.staticmap.StaticMapElementImageProvider;

/**
 * Created by germanosin on 17.06.14.
 */
public class PdfHandler implements GenericElementPdfHandler {

    private static final PdfHandler INSTANCE = new PdfHandler();

    public static PdfHandler getInstance() {
        return INSTANCE;
    }

    @Override
    public void exportElement(JRPdfExporterContext exporterContext,
                              JRGenericPrintElement element) {
        try {
            JRPdfExporter exporter = (JRPdfExporter) exporterContext.getExporter();
            JRPrintImage image = StaticMapElementImageProvider.getImage(
                    exporterContext.getJasperReportsContext(), element);
            exporter.exportImage(image);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public boolean toExport(JRGenericPrintElement element) {
        return true;
    }
}
