package org.gradoservice.jasperreports.staticmap;

import java.util.Map;

/**
 * Created by germanosin on 17.06.14.
 */
public enum StaticMapRequestParameter {
    serviceUrl(true, null),
    baseMap(true, "osm"),
    zoom(true, "1"),
    center(true, null),
    size(true, null),
    fitBounds(false, null),
    legend(false, null),
    URL_PARAMETERS(false, null);


    private boolean required = false;
    private final Object defaultValue;

    private StaticMapRequestParameter(boolean required, Object defaultValue) {
        this.required = required;
        this.defaultValue = defaultValue;
    }

    /**
     * Extracts an input parameter and returns it's value or a default value.
     */
    Object extract(Map<StaticMapRequestParameter, String> input) {
        Object value = input.get(this);
        if (value == null) {
            if (isRequired(input)) {
                throw new IllegalStateException(
                        String.format("Missing required parameter: %s", this.name()));
            }
            return defaultValue(input);
        }
        return value;
    }

    /**
     * Returns the default value for the parameter.
     */
    Object defaultValue(Map<StaticMapRequestParameter, String> input) {
        return defaultValue;
    }

    /**
     * Returns the actual HTTP parameter name.
     */
    String parameterName(Map<StaticMapRequestParameter, String> input) {
        return this.name();
    }

    /** Returns if the parameter is required. */
    boolean isRequired(Map<StaticMapRequestParameter, String> input) {
        return this.required;
    }


    public static StaticMapRequestParameter[] parameterValues() {
        return new StaticMapRequestParameter[] {
                baseMap,
                zoom,
                center,
                size,
                fitBounds,
                legend,
                URL_PARAMETERS
        };
    }
}
