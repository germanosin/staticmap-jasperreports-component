package org.gradoservice.jasperreports.staticmap;

import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.component.Component;
import net.sf.jasperreports.engine.component.ComponentKey;
import net.sf.jasperreports.engine.component.ComponentXmlWriter;
import net.sf.jasperreports.engine.type.EvaluationTimeEnum;
import net.sf.jasperreports.engine.util.JRXmlWriteHelper;
import net.sf.jasperreports.engine.util.VersionComparator;
import net.sf.jasperreports.engine.util.XmlNamespace;
import net.sf.jasperreports.engine.xml.JRXmlBaseWriter;
import net.sf.jasperreports.engine.xml.JRXmlConstants;
import net.sf.jasperreports.engine.xml.JRXmlWriter;
import org.gradoservice.jasperreports.staticmap.geojson.GeoJson;
import org.gradoservice.jasperreports.staticmap.geojson.GeoJsonData;
import org.gradoservice.jasperreports.staticmap.geojson.ItemProperty;

import java.io.IOException;
import java.util.List;

/**
 * Created by germanosin on 17.06.14.
 */
public class StaticMapComponentsXmlWriter implements ComponentXmlWriter {

    public static final String ATTRIBUTE_BASE_MAP = "baseMap";
    public static final String ATTRIBUTE_SERVICE_URL = "serviceUrl";
    public static final String ATTRIBUTE_FIT_BOUNDS = "fitBounds";
    public static final String ATTRIBUTE_LEGEND = "legend";
    public static final String ELEMENT_GEOJSON_DATA = "geoJsonData";
    public static final String ELEMENT_GEOJSON_ITEM = "geoItem";
    public static final String ELEMENT_ITEM_PROPERTY = "itemProperty";


    public static final String PROPERTY_COMPONENTS_PREFIX = JRPropertiesUtil.PROPERTY_PREFIX
            + "components.";

    public static final String PROPERTY_COMPONENTS_VERSION_SUFFIX = ".version";


    private final JasperReportsContext jasperReportsContext;
    private final VersionComparator versionComparator;

    public StaticMapComponentsXmlWriter(JasperReportsContext jasperReportsContext) {
        this.jasperReportsContext = jasperReportsContext;
        this.versionComparator = new VersionComparator();
    }

    @Override
    public void writeToXml(JRComponentElement componentElement,
                           JRXmlWriter reportWriter) throws IOException {
        Component component = componentElement.getComponent();
        if (component instanceof StaticMapComponent) {
            writeStaticMap(componentElement, reportWriter);
        }
    }

    protected void writeStaticMap(JRComponentElement componentElement,
                               JRXmlWriter reportWriter) throws IOException {
        Component component = componentElement.getComponent();
        StaticMapComponent map = (StaticMapComponent) component;
        JRXmlWriteHelper writer = reportWriter.getXmlWriteHelper();
        ComponentKey componentKey = componentElement.getComponentKey();

        XmlNamespace namespace = new XmlNamespace(
                ComponentsExtensionsRegistryFactory.NAMESPACE,
                componentKey.getNamespacePrefix(),
                ComponentsExtensionsRegistryFactory.XSD_LOCATION);

        writer.startElement("staticmap", namespace);


        if (map.getEvaluationTime() != EvaluationTimeEnum.NOW) {
            writer.addAttribute(JRXmlConstants.ATTRIBUTE_evaluationTime,
                    map.getEvaluationTime());
        }

        writer.addAttribute(JRXmlConstants.ATTRIBUTE_evaluationGroup,
                map.getEvaluationGroup());
        writer.addAttribute(ATTRIBUTE_SERVICE_URL, map.getServiceUrl());
        writer.addAttribute(ATTRIBUTE_BASE_MAP, map.getBaseMap());
        writer.addAttribute(ATTRIBUTE_FIT_BOUNDS, map.getFitBounds());
        writer.addAttribute(ATTRIBUTE_LEGEND, map.getLegend());


        writer.writeExpression("zoomExpression",
                map.getZoomExpression());
        writer.writeExpression("longitudeExpression",
                map.getLongitudeExpression());
        writer.writeExpression("latitudeExpression",
                map.getLatitudeExpression());

        List<GeoJsonData> geoJsonDataList = map.getGeoJsonDataList();
        if(geoJsonDataList !=null && geoJsonDataList.size() > 0) {
            for(GeoJsonData geoJsonData : geoJsonDataList) {
                writeItemData(ELEMENT_GEOJSON_DATA, geoJsonData, writer, reportWriter, namespace, componentElement);
            }
        }

        writer.closeElement();
    }

    private void writeItemData(String name, GeoJsonData itemData, JRXmlWriteHelper writer, JRXmlWriter reportWriter, XmlNamespace namespace, JRComponentElement componentElement) throws IOException
    {
        if (itemData != null)
        {
            writeItemDataContent(name, itemData, writer, reportWriter, namespace, componentElement);
            writer.closeElement();
        }
    }

    private void writeItemDataContent(String name, GeoJsonData itemData, JRXmlWriteHelper writer, JRXmlWriter reportWriter, XmlNamespace namespace, JRComponentElement componentElement) throws IOException
    {
        writer.startElement(name, namespace);

        JRElementDataset dataset = itemData.getDataset();
        if (dataset != null)
        {
            reportWriter.writeElementDataset(dataset, false);
        }

		/*   */
        List<GeoJson> itemList = itemData.getItems();
        if (itemList != null && !itemList.isEmpty())
        {
            for(GeoJson item : itemList)
            {
                if(item.getProperties() != null && !item.getProperties().isEmpty())
                {
                    writeItem(item, writer, reportWriter, namespace, componentElement);
                }
            }
        }
    }

    private void writeItem(GeoJson item, JRXmlWriteHelper writer, JRXmlWriter reportWriter, XmlNamespace namespace, JRComponentElement componentElement) throws IOException
    {
        writer.startElement(ELEMENT_GEOJSON_ITEM, namespace);
        List<ItemProperty> itemProperties = item.getProperties();
        for(ItemProperty property : itemProperties)
        {
            writeItemProperty(property, writer, reportWriter, namespace, componentElement);
        }
        writer.closeElement();
    }

    private void writeItemProperty(ItemProperty itemProperty, JRXmlWriteHelper writer, JRXmlWriter reportWriter, XmlNamespace namespace, JRComponentElement componentElement) throws IOException
    {
        writer.startElement(ELEMENT_ITEM_PROPERTY, namespace);
        writer.addAttribute(JRXmlConstants.ATTRIBUTE_name, itemProperty.getName());
        if(itemProperty.getValue() != null)
        {
            writer.addAttribute(JRXmlConstants.ATTRIBUTE_value, itemProperty.getValue());
        }
        writeExpression(JRXmlConstants.ELEMENT_valueExpression, JRXmlWriter.JASPERREPORTS_NAMESPACE, itemProperty.getValueExpression(), false, componentElement, reportWriter);
        writer.closeElement();
    }

    @Override
    public boolean isToWrite(JRComponentElement componentElement,
                             JRXmlWriter reportWriter)
    {
        ComponentKey componentKey = componentElement.getComponentKey();
        if (ComponentsExtensionsRegistryFactory.NAMESPACE.equals(componentKey
                .getNamespace())) {
            if (ComponentsExtensionsRegistryFactory.STATIC_MAP_COMPONENT_NAME
                    .equals(componentKey.getName())) {
                return isNewerVersionOrEqual(componentElement, reportWriter,
                        JRConstants.VERSION_5_0_4);
            }
        }
        // XXX: Defaults to true in jr ComponentsExtensionRegistryFactory???
        return false;
    }

    protected String getVersion(JRComponentElement componentElement,
                                JRXmlWriter reportWriter) {
        String version = null;

        ComponentKey componentKey = componentElement.getComponentKey();
        String versionProperty = PROPERTY_COMPONENTS_PREFIX
                + componentKey.getName() + PROPERTY_COMPONENTS_VERSION_SUFFIX;

        if (componentElement.getPropertiesMap().containsProperty(versionProperty)) {
            version = componentElement.getPropertiesMap()
                    .getProperty(versionProperty);
        } else {
            JRReport report = reportWriter.getReport();
            version = JRPropertiesUtil.getInstance(jasperReportsContext)
                    .getProperty(report, versionProperty);

            if (version == null) {
                version = JRPropertiesUtil.getInstance(jasperReportsContext)
                        .getProperty(report, JRXmlBaseWriter.PROPERTY_REPORT_VERSION);
            }
        }

        return version;
    }

    protected boolean isNewerVersionOrEqual(JRComponentElement componentElement,
                                            JRXmlWriter reportWriter, String oldVersion) {
        // FIXME VERSION can we pass something else then reportWriter?
        return versionComparator.compare(
                getVersion(componentElement, reportWriter), oldVersion) >= 0;
    }

    @SuppressWarnings("deprecation")
    protected void writeExpression(String name, JRExpression expression,
                                   boolean writeClass, JRComponentElement componentElement,
                                   JRXmlWriter reportWriter) throws IOException {
        JRXmlWriteHelper writer = reportWriter.getXmlWriteHelper();
        if (isNewerVersionOrEqual(componentElement, reportWriter,
                JRConstants.VERSION_4_1_1)) {
            writer.writeExpression(name, expression);
        } else {
            writer.writeExpression(name, expression, writeClass);
        }
    }

    @SuppressWarnings("deprecation")
    protected void writeExpression(String name, XmlNamespace namespace,
                                   JRExpression expression, boolean writeClass,
                                   JRComponentElement componentElement, JRXmlWriter reportWriter)
            throws IOException {
        JRXmlWriteHelper writer = reportWriter.getXmlWriteHelper();
        if (isNewerVersionOrEqual(componentElement, reportWriter,
                JRConstants.VERSION_4_1_1)) {
            writer.writeExpression(name, namespace, expression);
        } else {
            writer.writeExpression(name, namespace, expression, writeClass);
        }
    }
}
