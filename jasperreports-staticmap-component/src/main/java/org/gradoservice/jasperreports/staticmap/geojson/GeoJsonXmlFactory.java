package org.gradoservice.jasperreports.staticmap.geojson;

import net.sf.jasperreports.engine.xml.JRBaseFactory;
import org.xml.sax.Attributes;

/**
 * Created by germanosin on 19.06.14.
 */
public class GeoJsonXmlFactory extends JRBaseFactory
{
    public Object createObject(Attributes atts)
    {
        return new StandardGeoJson();
    }
}
