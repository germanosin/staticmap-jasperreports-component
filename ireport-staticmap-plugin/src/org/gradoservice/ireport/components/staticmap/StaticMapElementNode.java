/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.gradoservice.ireport.components.staticmap;

/**
 *
 * @author germanosin
 */
import com.jaspersoft.ireport.designer.ModelUtils;
import com.jaspersoft.ireport.designer.outline.nodes.ElementNode;
import com.jaspersoft.ireport.locale.I18n;
import net.sf.jasperreports.engine.design.JRDesignComponentElement;
import net.sf.jasperreports.engine.design.JRDesignDataset;
import net.sf.jasperreports.engine.design.JRDesignElement;
import net.sf.jasperreports.engine.design.JasperDesign;
import org.gradoservice.ireport.components.staticmap.properties.BaseMapProperty;
import org.gradoservice.ireport.components.staticmap.properties.EvaluationGroupProperty;
import org.gradoservice.ireport.components.staticmap.properties.EvaluationTimeProperty;
import org.gradoservice.ireport.components.staticmap.properties.LatitudeProperty;
import org.gradoservice.ireport.components.staticmap.properties.LongitudeProperty;
import org.gradoservice.ireport.components.staticmap.properties.ServiceUrlProperty;
import org.gradoservice.ireport.components.staticmap.properties.ZoomProperty;
import org.gradoservice.ireport.components.staticmap.properties.FitBoundsProperty;
import org.gradoservice.ireport.components.staticmap.properties.LegendProperty;
import org.gradoservice.jasperreports.staticmap.StandardStaticMapComponent;
import org.openide.nodes.Sheet;
import org.openide.util.Lookup;

/**
 * WMS map element node that creates the appropriate editor sheet to edit map
 * properties.
 */
public class StaticMapElementNode extends ElementNode {

  public StaticMapElementNode(JasperDesign jd, JRDesignElement element, Lookup doLkp) {
    super(jd, element, doLkp);
    setIconBaseWithExtension("org/gradoservice/ireport/components/staticmap/staticmap-16.png");
  }

  @Override
  public String getDisplayName() {
    return I18n.getString("StaticMapElementNode.name");
  }

  @Override
  protected org.openide.nodes.Sheet createSheet() {

    Sheet sheet = super.createSheet();

    // adding common properties...
    Sheet.Set propertySet = Sheet.createPropertiesSet();
    propertySet.setName("staticmap");
    StandardStaticMapComponent component = (StandardStaticMapComponent) ((JRDesignComponentElement) getElement()).getComponent();
    propertySet.setDisplayName(I18n.getString("staticmap"));

    JRDesignDataset dataset = ModelUtils.getElementDataset(getElement(), getJasperDesign());

    propertySet.put(new FitBoundsProperty(component));
    propertySet.put(new LegendProperty(component));
    propertySet.put(new ServiceUrlProperty(component));
    propertySet.put(new BaseMapProperty(component));   
    propertySet.put(new EvaluationTimeProperty(component,dataset));    
    propertySet.put(new EvaluationGroupProperty(component, dataset));
    propertySet.put(new ZoomProperty(component, dataset));
    propertySet.put(new LongitudeProperty(component, dataset));
    propertySet.put(new LatitudeProperty(component, dataset));

    sheet.put(propertySet);

    return sheet;
  }
}
