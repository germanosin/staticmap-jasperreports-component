package org.gradoservice.ireport.components.staticmap;

import com.jaspersoft.ireport.designer.palette.actions.CreateReportElementAction;
import com.jaspersoft.ireport.designer.utils.Misc;
import net.sf.jasperreports.engine.component.ComponentKey;
import net.sf.jasperreports.engine.design.JRDesignComponentElement;
import net.sf.jasperreports.engine.design.JRDesignElement;
import net.sf.jasperreports.engine.design.JasperDesign;
import org.gradoservice.jasperreports.staticmap.StandardStaticMapComponent;

/**
 * Created by germanosin on 17.06.14.
 */
public class CreateStaticMapAction extends CreateReportElementAction {

    @Override
    public JRDesignElement createReportElement(final JasperDesign jd) {
        JRDesignComponentElement component = new JRDesignComponentElement();
        StandardStaticMapComponent mapComponent = new StandardStaticMapComponent();
        mapComponent.setServiceUrl("http://188.226.194.176:9000/image");
        mapComponent.setBaseMap("osm");
        mapComponent.setZoomExpression(Misc.createExpression(null, "12"));
        mapComponent.setLongitudeExpression(Misc.createExpression(null, "49"));
        mapComponent.setLatitudeExpression(Misc.createExpression(null, "55"));

        component.setComponent(mapComponent);
        ComponentKey key = new ComponentKey("http://gradoservice.com/jasperreports/components", "gs", "staticmap");
        component.setComponentKey(key);
        component.setWidth(200);
        component.setHeight(200);
        return component;
    }
}
