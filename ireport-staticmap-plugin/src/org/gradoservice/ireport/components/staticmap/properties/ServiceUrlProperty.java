/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.gradoservice.ireport.components.staticmap.properties;

import com.jaspersoft.ireport.designer.sheet.properties.StringProperty;
import com.jaspersoft.ireport.locale.I18n;
import org.gradoservice.jasperreports.staticmap.StandardStaticMapComponent;
/**
 *
 * @author germanosin
 */
public class ServiceUrlProperty extends StringProperty {
    private final StandardStaticMapComponent component;

  public ServiceUrlProperty(StandardStaticMapComponent component) {
    super(component);
    this.component = component;
    setName(StandardStaticMapComponent.PROPERTY_SERVICE_URL);
    setDisplayName(I18n.getString("Global.Property.ServiceUrl"));
    setShortDescription(I18n.getString("Global.Property.ServiceUrl.desc"));
  }

  @Override
  public String getString() {
    return (this.component.getServiceUrl() == null) ?
            "" : this.component.getServiceUrl();
  }

  @Override
  public String getOwnString() {
    return this.component.getServiceUrl();
  }

  @Override
  public String getDefaultString() {
    return "http://";
  }

  @Override
  public void setString(String value) {
    this.component.setServiceUrl(value);
  }

  @Override
  public void validate(Object value) {

  }

  @Override
  public boolean supportsDefaultValue() {
    return false;
  }
}
