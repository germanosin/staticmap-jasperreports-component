/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.gradoservice.ireport.components.staticmap.properties;

import com.jaspersoft.ireport.designer.sheet.properties.ExpressionProperty;
import com.jaspersoft.ireport.locale.I18n;
import net.sf.jasperreports.engine.design.JRDesignDataset;
import net.sf.jasperreports.engine.design.JRDesignExpression;
import org.gradoservice.jasperreports.staticmap.StandardStaticMapComponent;

/**
 *
 * @author germanosin
 */
public class ZoomProperty extends ExpressionProperty {
    private final StandardStaticMapComponent component;

  public ZoomProperty(StandardStaticMapComponent component, JRDesignDataset dataset) {
    super(component, dataset);
    this.component = component;
  }

  @Override
  public String getName() {
    return StandardStaticMapComponent.PROPERTY_ZOOM_EXPRESSION;
  }

  @Override
  public String getDisplayName() {
    return I18n.getString("Global.Property.ZoomExpression");
  }

  @Override
  public String getShortDescription() {
    return I18n.getString("Global.Property.ZoomExpression.desc");
  }

  @Override
  public String getDefaultExpressionClassName() {
    return String.class.getName();
  }

  @Override
  public JRDesignExpression getExpression() {
    return (JRDesignExpression) component.getZoomExpression();
  }

  @Override
  public void setExpression(JRDesignExpression expression) {
    component.setZoomExpression(expression);
  }    
}
