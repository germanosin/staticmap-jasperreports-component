/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.gradoservice.ireport.components.staticmap.properties;

import com.jaspersoft.ireport.designer.sheet.properties.StringProperty;
import com.jaspersoft.ireport.locale.I18n;
import org.gradoservice.jasperreports.staticmap.StandardStaticMapComponent;

/**
 *
 * @author germanosin
 */
public class BaseMapProperty extends StringProperty {
    private final StandardStaticMapComponent component;

  public BaseMapProperty(StandardStaticMapComponent component) {
    super(component);
    this.component = component;
    setName(StandardStaticMapComponent.PROPERTY_BASEMAP);
    setDisplayName(I18n.getString("Global.Property.BaseMap"));
    setShortDescription(I18n.getString("Global.Property.BaseMap.desc"));
  }

  @Override
  public String getString() {
    return (this.component.getBaseMap() == null) ? 
            "" : this.component.getBaseMap();
  }

  @Override
  public String getOwnString() {
    return this.component.getBaseMap();
  }

  @Override
  public String getDefaultString() {
    return "http://";
  }

  @Override
  public void setString(String value) {
    this.component.setBaseMap(value);
  }

  @Override
  public void validate(Object value) {
    
  }

  @Override
  public boolean supportsDefaultValue() {
    return false;
  }    
}
