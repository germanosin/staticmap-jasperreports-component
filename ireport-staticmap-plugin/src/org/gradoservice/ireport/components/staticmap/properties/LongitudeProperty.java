/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.gradoservice.ireport.components.staticmap.properties;

import com.jaspersoft.ireport.designer.sheet.properties.ExpressionProperty;
import com.jaspersoft.ireport.locale.I18n;
import net.sf.jasperreports.engine.design.JRDesignDataset;
import net.sf.jasperreports.engine.design.JRDesignExpression;
import org.gradoservice.jasperreports.staticmap.StandardStaticMapComponent;

/**
 *
 * @author germanosin
 */
public class LongitudeProperty extends ExpressionProperty {
    private final StandardStaticMapComponent component;

  public LongitudeProperty(StandardStaticMapComponent component, JRDesignDataset dataset) {
    super(component, dataset);
    this.component = component;
  }

  @Override
  public String getName() {
    return StandardStaticMapComponent.PROPERTY_LONGTITUDE_EXPRESSION;
  }

  @Override
  public String getDisplayName() {
    return I18n.getString("Global.Property.LongitudeExpression");
  }

  @Override
  public String getShortDescription() {
    return I18n.getString("Global.Property.LongitudeExpression.desc");
  }

  @Override
  public String getDefaultExpressionClassName() {
    return String.class.getName();
  }

  @Override
  public JRDesignExpression getExpression() {
    return (JRDesignExpression) component.getLongitudeExpression();
  }

  @Override
  public void setExpression(JRDesignExpression expression) {
    component.setLongitudeExpression(expression);
  }
}
