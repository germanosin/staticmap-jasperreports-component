/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.gradoservice.ireport.components.staticmap.properties;

import com.jaspersoft.ireport.designer.sheet.properties.BooleanProperty;
import com.jaspersoft.ireport.locale.I18n;
import org.gradoservice.jasperreports.staticmap.StandardStaticMapComponent;


/**
 *
 * @author germanosin
 */
public class FitBoundsProperty  extends BooleanProperty {
    private final StandardStaticMapComponent component;

  public FitBoundsProperty(StandardStaticMapComponent component) {
    super(component);
    this.component = component;
    setName(StandardStaticMapComponent.PROPERTY_FIT_BOUNDS);
    setDisplayName(I18n.getString("Global.Property.FitBounds"));
    setShortDescription(I18n.getString("Global.Property.FitBounds.desc"));
  }

  @Override
  public Boolean getBoolean() {
    return (this.component.getFitBounds() == null) ?
            false : this.component.getFitBounds();
  }

  @Override
  public Boolean getOwnBoolean() {
    return this.component.getFitBounds();
  }

  @Override
  public Boolean getDefaultBoolean() {
    return false;
  }

  @Override
  public void setBoolean(Boolean value) {
    this.component.setFitBounds(value);
  }

  @Override
  public void validate(Object value) {

  }

  @Override
  public boolean supportsDefaultValue() {
    return false;
  }
}
