/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.gradoservice.ireport.components.staticmap.properties;

/**
 *
 * @author germanosin
 */
import com.jaspersoft.ireport.designer.sheet.properties.ExpressionProperty;
import com.jaspersoft.ireport.locale.I18n;
import net.sf.jasperreports.engine.design.JRDesignDataset;
import net.sf.jasperreports.engine.design.JRDesignExpression;
import org.gradoservice.jasperreports.staticmap.StandardStaticMapComponent;

/**
 *
 * @author germanosin
 */
public class LatitudeProperty extends ExpressionProperty {
    private final StandardStaticMapComponent component;

  public LatitudeProperty(StandardStaticMapComponent component, JRDesignDataset dataset) {
    super(component, dataset);
    this.component = component;
  }

  @Override
  public String getName() {
    return StandardStaticMapComponent.PROPERTY_LATITUDE_EXPRESSION;
  }

  @Override
  public String getDisplayName() {
    return I18n.getString("Global.Property.LatitudeExpression");
  }

  @Override
  public String getShortDescription() {
    return I18n.getString("Global.Property.LatitudeExpression.desc");
  }

  @Override
  public String getDefaultExpressionClassName() {
    return String.class.getName();
  }

  @Override
  public JRDesignExpression getExpression() {
    return (JRDesignExpression) component.getLatitudeExpression();
  }

  @Override
  public void setExpression(JRDesignExpression expression) {
    component.setLatitudeExpression(expression);
  }
}