/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.gradoservice.ireport.components.staticmap.properties;

import com.jaspersoft.ireport.designer.sheet.properties.BooleanProperty;
import com.jaspersoft.ireport.locale.I18n;
import org.gradoservice.jasperreports.staticmap.StandardStaticMapComponent;


/**
 *
 * @author germanosin
 */
public class LegendProperty  extends BooleanProperty {
    private final StandardStaticMapComponent component;

  public LegendProperty(StandardStaticMapComponent component) {
    super(component);
    this.component = component;
    setName(StandardStaticMapComponent.PROPERTY_LEGEND);
    setDisplayName(I18n.getString("Global.Property.Legend"));
    setShortDescription(I18n.getString("Global.Property.Legend.desc"));
  }

  @Override
  public Boolean getBoolean() {
    return (this.component.getLegend() == null) ?
            false : this.component.getLegend();
  }

  @Override
  public Boolean getOwnBoolean() {
    return this.component.getLegend();
  }

  @Override
  public Boolean getDefaultBoolean() {
    return false;
  }

  @Override
  public void setBoolean(Boolean value) {
    this.component.setLegend(value);
  }

  @Override
  public void validate(Object value) {

  }

  @Override
  public boolean supportsDefaultValue() {
    return false;
  }
}
