/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.gradoservice.ireport.components.staticmap;

import com.jaspersoft.ireport.designer.AbstractReportObjectScene;
import com.jaspersoft.ireport.designer.ElementNodeFactory;
import com.jaspersoft.ireport.designer.outline.nodes.ElementNode;
import com.jaspersoft.ireport.designer.widgets.JRDesignElementWidget;
import net.sf.jasperreports.engine.design.JRDesignComponentElement;
import net.sf.jasperreports.engine.design.JRDesignElement;
import net.sf.jasperreports.engine.design.JasperDesign;
import org.openide.util.Lookup;

/**
 *
 * @author germanosin
 */
public class StaticMapElementNodeFactory implements ElementNodeFactory {

  @Override
  public ElementNode createElementNode(JasperDesign jd, JRDesignComponentElement element, Lookup lkp) {
    return new StaticMapElementNode(jd, element, lkp);
  }

  @Override
  public JRDesignElementWidget createElementWidget(AbstractReportObjectScene scene, JRDesignElement element) {
    return new StaticMapComponentWidget(scene, element);

  }
}
