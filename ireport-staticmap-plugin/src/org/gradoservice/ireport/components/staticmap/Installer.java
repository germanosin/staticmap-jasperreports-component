/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.gradoservice.ireport.components.staticmap;

import com.jaspersoft.ireport.locale.I18n;
import java.util.ResourceBundle;
import org.openide.modules.ModuleInstall;

/**
 *
 * @author germanosin
 */
public class Installer extends ModuleInstall {

  @Override
  public void restored() {
    I18n.addBundleLocation(ResourceBundle
        .getBundle("/org/gradoservice/ireport/components/staticmap/Bundle"));
  }
}
